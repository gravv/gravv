import { configDefaults, defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    exclude: [...configDefaults.exclude],
    include: ['src/**/*.spec.js'],
    coverage: {
      exclude: [
	// config
	'.vitest.config.*',
	'eslint.config.js',
	'src/**/*.spec.js',
	'knexfile.js',
	// scripts (maybe should test)
	'scripts/**',
	// migrations
	'migrations/**',
	// test input files
	'samples/**',
	// infra services
	'src/common/kafka/index.js',
	// entrypoints
	'src/apps/**/*',
      ]
    }
  },
});
