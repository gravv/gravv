# FAQ

## [Error: EACCES: permission denied, mkdir '/storage']
Solved in the container environments by making storage under the user folder.

```
  "hubStorage": {
    "files": "/home/node/storage/fileblobs",
  },
```

## [BrokerPool] Failed to connect to seed broker
[BrokerPool] Failed to connect to seed broker, trying another broker from the list: Connection error: connect ECONNREFUSED 172.20.0.10:9092

Solved in the container environments by putting all the containers under the same network.

```
    networks:
      test:
        ipv4_address: 172.20.0.30
```

## Kafka error: unhandledRejection KafkaJSProtocolError: This server does not host this topic-partition
Service: hub

## Unable to register broker 1 because the controller returned error DUPLICATE_BROKER_REGISTRATION
Theory: docker-compose has the broker id hardcoded. when restarting it will have a duplicate id from the last run.

## Where are the kafka properties
/opt/bitnami/kafka/config/server.properties

## KafkaJS v2.0.0 switched default partitioner
Nothing to do. The warning should go away in a future version. We can disable it with an environment variable KAFKAJS_NO_PARTITIONER_WARNING=1.

## List existing topics
docker exec -it smoke-test-kafka /bin/bash
/opt/bitnami/kafka/bin/kafka-topics.sh --bootstrap-server=0.0.0.0:9092 --list
