/* eslint-disable */
import dotenv from 'dotenv';
import fs from 'fs';
import knex from 'knex';
import _ from 'lodash';
import { knexSnakeCaseMappers } from 'objection';
import readline from 'readline';

import {
  earliestDate,
  toNearestMinute,
} from '../src/common/lib/index.js';

import {
  bufferTypesSupported,
  exifExposureModes,
  exifGPSEnum,
  exifMeteringModes,
  ffmpegCodecs,
  ffmpegFormats,
  imagemagickOrientationEnum,
} from '../src/constants/files/index.js';

dotenv.config({ path: `./.env-${process.env.NODE_ENV}` });

const applyFraction = (value) => {
  if (!value?.length) {
    return undefined;
  }
  if (!/^\s+\d+\s+\/\s+\d+\s+$/.test(value)) {
    return +value;
  }

  const [numerator, denominator] = value.split('/');

  return +numerator / +denominator;
};

const cleanExtension = (value) => {
  if (value === 'jpeg') {
    return 'jpg';
  }
  return value;
};

const toISODate = (value) => {
  if (value instanceof Date) {
    return value.toISOString();
  }
  if (!value?.length) {
    return undefined;
  }
  value = value.trim();
  const dateFormat = /^\d?\d\/\d\d\/\d\d\d\d \d\d:\d\d:\d\d$/;
  const hyphenFormat = /^\d?\d-\d\d-\d\d\d\d \d\d:\d\d:\d\d$/;
  const zeroDate = /^0000:00:00 00:00:00$/;

  if (dateFormat.test(value)) {
    return new Date(value.replace(/\//g, '-')).toISOString();
  }
  if (hyphenFormat.test(value)) {
    return new Date(value).toISOString();
  }
  if (zeroDate.test(value) || isNaN(Date.parse(value))) {
    return undefined;
  }
  return new Date(value).toISOString();
};

const toBool = (value) => {
  const isTrue = (value.length ? value : '').trim() === 'true';
  const isFalse = (value.length ? value : '').trim() === 'true';
  if (!value || !(isTrue || isFalse)) {
    return undefined;
  }
  return isTrue;
};

const toNumber = (value) => {
  if (!value || isNaN(+value)) return undefined;
  return +value;
};

const toFilename = (value) => {
  if (!value) return undefined;
  return value.replace(/\.([a-z]+)$/i, (_, ext) => `.${ext.toLowerCase()}`);
};

const toHexLong = (value) => {
  if (!value || !/^[0-9A-Fa-f]{20}[0-9A-Fa-f]+$/.test(value)) return undefined;
  return value;
};

const toFileSizeNumber = (value) => {
  const match = value.match(/^(\d+)\s?MB$/i);
  if (match) {
    return Number(match[1]) * 1024 * 1024;
  }
  return undefined;
};

const naToUndefined = (value) => {
  if (value?.length && value.trim().toUpperCase() === 'N/A') {
    return undefined;
  }
  return value;
};

const toEnum = (value, enumValues) => {
  if (!value || !enumValues.includes(value)) return undefined;
  return value;
};

const toPath = (value) => {
  if (!value || !/^(?:\/|[a-zA-Z]:\\)[\S\s]*$/.test(value)) return undefined;
  return value;
};

const toAlphanumeric = (value) => {
  if (!value || !/^[a-zA-Z0-9]+$/.test(value)) return undefined;
  return value;
};

const toAlphanumericHyphen = (value) => {
  if (!value || !/^[a-zA-Z0-9\-]+$/.test(value)) return undefined;
  return value;
};

// Configure Knex connection
const db = knex({
  client: 'pg',
  connection: `postgres://${process.env.POSTGRES_USER}:${process.env.POSTGRES_PASSWORD}@127.0.0.1:48000/media`,
  pool: { min: 0, max: 10 },
  acquireConnectionTimeout: 10000,
  ...knexSnakeCaseMappers(),
});

await db('media').truncate();

// Setup the readline interface
const fileStream = fs.createReadStream('/home/steven/data/gravv/results.ndjson');
const rl = readline.createInterface({
  input: fileStream,
  crlfDelay: Infinity
});

const typeMap = {};

// Function to map and insert data
async function insertData(line) {
  let fileData = {}
  try {
    const data = JSON.parse(line);
    fileData = {
      filename: toFilename(data.filenameCleaned ?? data.exifFileName),
      size: toNumber(data.statSize),
      type: toEnum(cleanExtension(data.exifFileType), bufferTypesSupported),
      createdAt: toISODate(earliestDate(data, { minStartDate: '1990-01-01' })),
      modifiedAt: new Date(), // Current date as the modified timestamp
      hash: toHexLong(data.hash),
      identity: {
        ext: toEnum(cleanExtension(data.identityExt), bufferTypesSupported),
        file: toPath(data.identityFile),
        sha256: toHexLong(data.identitySha256),
        signature: toHexLong(data.identitySignature),
        machineId: toHexLong(data.identityMachineId)
      },
      exif: {
        createDate: toISODate(data.exifCreateDate),
        dateTimeOriginal: toISODate(data.exifDateTimeOriginal),
        fileAccessDate: toISODate(data.exifFileAccessDate),
        fileModifyDate: toISODate(data.exifFileModifyDate),
        fileName: toFilename(data.exifFileName),
        fileSize: toFileSizeNumber(data.exifFileSize),
        fileType: toEnum(cleanExtension(data.exifFileType), bufferTypesSupported),
        imageHeight: toNumber(data.exifImageHeight),
        imageWidth: toNumber(data.exifImageWidth),
        megapixels: toNumber(data.exifMegapixels),
        modifyDate: toISODate(data.exifModifyDate),
        sourceFile: toPath(data.exifSourceFile),
        userComment: data.exifUserComment, // No processing
        exifVersion: toNumber(data.exifExifVersion),
        resolutionUnit: data.exifResolutionUnit, // No processing
        xResolution: toNumber(data.exifXResolution),
        yResolution: toNumber(data.exifYResolution),
        aperture: toNumber(data.exifAperture),
        fNumber: toNumber(data.exifFNumber),
        focalLength: data.exifFocalLength, // No processing function specified
        iso: toNumber(data.exifISO),
        model: data.exifModel, // No processing function specified
        shutterSpeed: data.exifShutterSpeed, // No processing function specified
        exposureCompensation: toNumber(data.exifExposureCompensation),
        exposureMode: toEnum(data.exifExposureMode, exifExposureModes),
        exposureProgram: data.exifExposureProgram, // No processing function specified
        meteringMode: toEnum(data.exifMeteringMode, exifMeteringModes),
        orientation: toNumber(data.exifOrientation),
        gpsLatitude: toNumber(data.exifGPSLatitude),
        gpsLongitude: toNumber(data.exifGPSLongitude),
        gpsAltitude: toNumber(data.exifGPSAltitude),
        gpsLatitudeRef: toEnum(data.exifGPSLatitudeRef, exifGPSEnum),
        gpsLongitudeRef: toEnum(data.exifGPSLongitudeRef, exifGPSEnum),
        author: data.exifAuthor, // No processing function specified
        title: data.exifTitle // No processing function specified
      },
      ffmpeg: {
        bitRate: toNumber(naToUndefined(data.ffmpegBitRate)),
        codecName: toEnum(data.ffmpegCodecName, ffmpegCodecs),
        duration: toNumber(naToUndefined(data.ffmpegDuration)),
        formatName: toEnum(data.ffmpegFormatName, ffmpegFormats),
        frameRate: applyFraction(data.ffmpegFrameRate),
        height: toNumber(data.ffmpegHeight),
        nbStreams: toNumber(data.ffmpegNbStreams),
        probeScore: toNumber(data.ffmpegProbeScore),
        width: toNumber(data.ffmpegWidth),
        colorSpace: data.ffmpegColorSpace // No processing function specified
      },
      imagemagick: {
        depth: toNumber(data.imagemagickDepth),
        entropy: toNumber(data.imagemagickEntropy),
        height: toNumber(data.imagemagickHeight),
        kurtosis: toNumber(data.imagemagickKurtosis),
        mean: data.imagemagickMean, // No processing function specified
        signature: toHexLong(data.imagemagickSignature),
        skewness: toNumber(data.imagemagickSkewness),
        width: toNumber(data.imagemagickWidth),
        quality: toNumber(data.imagemagickQuality),
        orientation: toEnum(data.imagemagickOrientation, imagemagickOrientationEnum)
      },
      stat: {
        atime: toISODate(data.statAtime),
        birthtime: toISODate(data.statBirthtime),
        ctime: toISODate(data.statCtime),
        gid: toNumber(data.statGid),
        isDirectory: toBool(data.statIsDirectory),
        isFile: toBool(data.statIsFile),
        isSymbolicLink: toBool(data.statIsSymbolicLink),
        mode: toNumber(data.statMode),
        mtime: toISODate(data.statMtime),
        size: toNumber(data.statSize),
        uid: toNumber(data.statUid)
      },
      legacyIdentifiers: {
        batchId: data.batchId, // No processing function specified
        _idPrev: toHexLong(data._idPrev),
        hashPrev: toHexLong(data.hashPrev),
        hashShort: toAlphanumeric(data.hashShort),
        outputPath: toFilename(data.outputPath),
        __dfNameStripped: toAlphanumeric(data.__dfNameStripped),
        __dfNameStripped2: toAlphanumericHyphen(data.__dfNameStripped2),
        __filename: toFilename(data.__filename),
        __dfName: data.__dfName, // No processing function specified
        __dfDate: data.__dfDate, // No processing function specified
        __dfHash: data.__dfHash, // No processing function specified
        __ext: data.__ext, // No processing function specified
        dfHashPrev: data.dfHashPrev, // No processing function specified
        dfDatePrev: data.dfDatePrev, // No processing function specified
        dfNamePrev: data.dfNamePrev, // No processing function specified
        name2018: data.name2018, // No processing function specified
        filenameCleaned: toFilename(data.filenameCleaned),
        _id: toHexLong(data._id),
        thumbPrefix: toFilename(data.thumbPrefix),
        path: toPath(data.path),
      },
    };

    // Insert into database
    await db('media').insert(fileData);
    console.log('Data inserted');
  } catch (error) {
    console.error('Error inserting data:', error);
    console.error('Data causing error:', fileData);
    process.exit(1)
  }
}

function updateTypeMap(key, value) {
  if (!typeMap[key]) {
    typeMap[key] = { counts: {}, examples: {}, enums: new Set() };
  }

  const entry = typeMap[key];

  const enumKeys = [
    'exifFileType', 'identityExt', 'exifExposureMode',
    'exifMeteringMode', 'exifGPSLatitudeRef', 'exifGPSLongitudeRef',
    'ffmpegCodecName', 'ffmpegFormatName', 'imagemagickOrientation'
  ];

  if (enumKeys.includes(key)) {
    entry.enums.add(value);
  }

}

db.on('query-error', (error, obj) => {
  console.error('Query Error:', error);
});

// Read the NDJSON file line by line
rl.on('line', (line) => {
  insertData(line);
  // const data = JSON.parse(line);
  // _.forEach(data, (value, key) => {
  //   updateTypeMap(key, value);
  // });
});

// Close the database connection when done
rl.on('close', () => {
  console.log('Finished processing file.');
  console.log(typeMap)
  // Object.entries(typeMap).forEach(([key, {counts, examples}]) => {
  //   console.log(key + ': ')
  //   // console.log(JSON.stringify(counts, null, 2))
  //   Object.entries(examples).forEach(([type, exampleCounts]) => {
  //     // Sort by count and slice to get top three
  //     const topThree = Object.entries(exampleCounts)
  //           .sort((a, b) => b[1] - a[1])
  //           .slice(0, 1)
  //           .map(([example, count]) => `${example}`) // : ${count}`);
  //     console.log(` - ${type}: ${topThree.join(', ')}`);
  //   });
  // });
});






// function updateTypeMap(key, value) {
//     if (!typeMap[key]) {
//         typeMap[key] = { counts: {}, examples: {} };
//     }

//     const entry = typeMap[key];
//     let valueType = typeof value;

//     // Handle null, array, NaN, and undefined cases
//     if (value === null) {
//         valueType = 'null';
//     } else if (Array.isArray(value)) {
//         valueType = 'array';
//     } else if (valueType === 'number' && isNaN(value)) {
//         valueType = 'NaN';
//     } else if (valueType === 'undefined') {
//         valueType = 'undefined';
//     }

//     // Define patterns in a prioritized order
//     const patterns = [
//       // Date formats
//       ['isoDateStringValid', /^(19[89]\d|20[012]\d|2030)-([0][1-9]|1[012])-([012]\d|3[01])T([01]\d|2[0-3]):([0-5]\d):([0-5]\d)(\.\d+)?(Z|[+-]([01]\d|2[0-3]):[0-5]\d)$/],
//       ['isoDateStringZero', /0000:00:00 00:00:00/],
//       ['isoDateStringInvalidDate', /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/],
//       ['dateString', /^(?:0[1-9]|1[0-2])\/(?:0[1-9]|[12][0-9]|3[01])\/(?:19|20\d{2})$/],
//       ['dateBasicStringWithTime', /^[12]\d{7}-\d{6} \d\d:\d\d:\d\d$/],
//       ['dateBasicString', /^[12]\d{7}-\d{6}$/],

//       // File paths and filenames
//       ['filePath', /^(?:\/|[a-zA-Z]:\\)[\S\s]*$/],
//       ['filename', /[^\\/]+\.(mp4|gif|png|jpg|mov|mp3|wav|txt|pdf|mkv|jpeg|tif|mjpeg|h264|dng|mpeg)$/],
//       ['filenameBadCaps', /[^\\/]+\.(mp4|gif|png|jpg|mov|mp3|wav|txt|pdf|mkv|jpeg|tif|mjpeg|h264|dng|mpeg)$/i],

//       // Numeric formats
//       ['numericDecimalString', /^-?\d+\.\d+$/],
//       ['numericDecimalPairString', /^-?\d+\.\d+ \(-?\d+\.\d+\)$/],
//       ['numericFractionString', /^(-|\+)?\d+\/\d+$/],
//       ['numericPlusDecimalString', /^(-|\+)?\d+\.\d+$/],
//       ['numericPlusString', /^\+\d+$/],
//       ['numericSmallDecimalString', /^-?\d+\.\d+e-\d{2}$/],
//       ['numericDecimalUnitString', /^-?\d+\.\d+ [a-z]{2}$/],

//       // Miscellaneous formats

//       ['na', /^N\/A$/],
//       ['unknown', /^Unknown$/],
//       ['boolean', /^(true|false)$/],
//       ['none', /^None$/],
//       ['units', /^(inches|cm)$/],
//       ['directions', /^(N|E|S|W)$/],
//       ['url', /^https?:\/\/[^\s$.?#].[^\s]*$/],
//       ['fileSize', /^(\d+\s?(?:KB|MB|GB|TB|bytes))$/i],
//       ['dimensions', /^(\d+)x(\d+)$/],
//       ['aspectRatio', /^\d{1,2}:\d{1,2}$/],
//       ['cameraInfo', /^ISO\s\d+|f\/\d+(\.\d+)?|1\/\d+\ssec$/],
//       ['twoNumeric', /^-?\d{1,3}\.\d+,\s*-?\d{1,3}\.\d+$/],
//       ['posTBLR', /^(TopRight|TopLeft|BottomRight|BottomLeft|RightTop|RightBottom|LeftTop|LeftBottom)$/],
//       ['ext', /^(mp4|gif|png|jpg|mov|mp3|wav|txt|pdf|mkv|jpeg|tif|mjpeg|h264|dng|mpeg)$/],

//       // String patterns
//       ['emptyString', /^$/],
//       ['hexShort', /^[0-9A-Fa-f]{6,20}$/],
//       ['alphanumericLength6', /^[a-zA-Z0-9]{6}$/],
//       ['numericString', /^-?\d+$/],
//       ['hexLong', /^[0-9A-Fa-f]{20}[0-9A-Fa-f]+$/],
//       ['hex', /^[0-9A-Fa-f]+$/],
//       ['alphanumeric', /^[a-zA-Z0-9]+$/],
//       ['alphanumericHyphen', /^[a-zA-Z0-9\-]+$/],
//       ['stringWithSpaces', /^[a-zA-Z0-9\s]+$/],
//     ];

//     let matchedSpecial = false;

//     // Iterate over each pattern and test it against the value
//     patterns.forEach(([patternKey, regex]) => {
//         if (regex.test(value) && !matchedSpecial) {
//             entry.counts[patternKey] = (entry.counts[patternKey] || 0) + 1;
//             matchedSpecial = true;
//             // Update examples for specific patterns
//             if (!entry.examples[patternKey]) {
//                 entry.examples[patternKey] = {};
//             }
//             entry.examples[patternKey][value] = (entry.examples[patternKey][value] || 0) + 1;
//         }
//     });

//     // Increment the count for generic types if no specific type has matched
//     if (!matchedSpecial) {
//         entry.counts[valueType] = (entry.counts[valueType] || 0) + 1;
//         if (!entry.examples[valueType]) {
//             entry.examples[valueType] = {};
//         }
//         entry.examples[valueType][value] = (entry.examples[valueType][value] || 0) + 1;
//     }
// }
