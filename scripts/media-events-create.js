import { makeClient } from '../src/common/kafka/index.js';
import {
  mediaEventConfig,
} from '../src/constants/kafka-topics/index.js';

const client = makeClient();

const admin = client.admin();
const run = async () => {
  await admin.connect();

  await admin.createTopics({
    topics: [mediaEventConfig.topic]
  });

  await admin.disconnect();
};

run();
