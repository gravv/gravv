/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = (knex) => {
  return knex.schema.createTable('media', function(table) {
    table.increments('id').primary();
    table.string('filename').defaultTo('');
    table.string('hash').notNullable().unique();
    table.bigint('size').defaultTo(0);
    table.string('type', 50).defaultTo('');
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('modified_at').defaultTo(knex.fn.now());
    table.timestamp('deleted_at').nullable();
    // JSONB columns for extended metadata;
    table.jsonb('exif').defaultTo('{}');
    table.jsonb('ffmpeg').defaultTo('{}');
    table.jsonb('imagemagick').defaultTo('{}');
    table.jsonb('stat').defaultTo('{}');
    table.jsonb('identity').defaultTo('{}');
    table.jsonb('legacy_identifiers').defaultTo('{}');
    table.jsonb('artifacts').defaultTo('{}');
    table.jsonb('snippets').defaultTo('{}');
    table.jsonb('notes').defaultTo('{}');
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = (knex) => {
  return knex.schema.dropTable('media');
};
