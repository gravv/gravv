ARG DEBIAN_VERSION=bullseye-slim

###########################################
# Base stage with required packages
###########################################
FROM debian:${DEBIAN_VERSION} AS base

ARG NODE_VERSION=20
ENV UID=1000
ENV GID=1000

WORKDIR /app

# Software needed by the workers
RUN apt-get update && apt-get install -y --no-install-recommends \
    curl \
    ca-certificates \
    git \
    gnupg \
    gosu \
    nano \
    ffmpeg \
    graphicsmagick \
    rsync \
    libimage-exiftool-perl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install Node.js
RUN curl -sL "https://deb.nodesource.com/setup_${NODE_VERSION}.x" | bash - && \
    apt-get update && apt-get install -y --no-install-recommends \
    nodejs && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Create node user and set up directories
RUN groupadd -g ${GID} node && \
    useradd -u ${UID} -g node -m node && \
    umask 0077 \
    mkdir -p /app && \
    mkdir -p /home/node/.npm

COPY ./docker/media/entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

###########################################
# Production stage
###########################################
FROM base AS production

# Copy package.json and package-lock.json
COPY package*.json ./
RUN npm ci --only=production
COPY . .
CMD ["npm", "run", "worker:production"]

###########################################
# Development stage
###########################################
FROM base AS development

# Install development tools as root
RUN apt-get update && apt-get install -y --no-install-recommends \
    net-tools \
    lsof \
    vim \
    wget \
    iputils-ping \
    dnsutils \
    telnet \
    tcpdump \
    build-essential && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY package*.json ./
RUN npm ci
COPY . .
CMD ["npm", "run", "worker:development"]