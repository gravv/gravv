#!/bin/bash

# Ensure the script exits if any command fails
set -e

change_ownership() {
  local dir=$1
  if [ -d "$dir" ]; then
    chown -R $GOSU_USER $dir || echo "Failed to change ownership of $dir"
  fi
}

change_permissions() {
  local dir=$1
  if [ -d "$dir" ]; then
    chmod -R 700 $dir || echo "Failed to change permissions of $dir"
  fi
}

dirs=(
  "/home/node/.npm"
)

# Loop through the list and change ownership if the directory exists
for dir in "${dirs[@]}"; do
  mkdir -p $dir
  change_ownership $dir
  change_permissions $dir
done

# If GOSU_CHOWN environment variable set, recursively chown all specified directories
# to match the user:group set in GOSU_USER environment variable.
if [ -n "$GOSU_CHOWN" ]; then
    for dir in $GOSU_CHOWN
    do
        mkdir -p $dir
        chown -R $GOSU_USER $dir
        change_permissions $dir
    done
fi

# If GOSU_USER environment variable set to something other than 0:0 (root:root),
# become user:group set within and exec command passed in args
if [ "$GOSU_USER" != "0:0" ]; then
    exec gosu $GOSU_USER bash -c 'export TZ=$(cat /etc/timezone) && exec "$@"' -- "$@"
fi

# If GOSU_USER was 0:0 exec command passed in args without gosu (assume already root)
exec "$@"
