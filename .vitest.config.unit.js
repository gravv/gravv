import { configDefaults, defineConfig } from 'vitest/config';

export default defineConfig({
	test: {
    exclude: [...configDefaults.exclude, 'src/**/*.hub-api.spec.js', 'src/**/*.hub-worker.spec.js', 'src/**/*.worker.spec.js', 'src/**/*.system.spec.js'],
		include: ['src/**/*.spec.js']
	},
});