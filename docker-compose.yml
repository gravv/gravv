services:
  api:
    container_name: api-${NODE_ENV}
    build:
      context: .
      dockerfile: ./docker/minimal/Dockerfile
      target: ${BUILD_ENV:-production}
    image: registry.gitlab.com/far-world-labs/gravv/api:latest
    command: npm run api:${NODE_ENV}
    environment:
      - GOSU_USER=${UID:-1000}:${GID:-1000}
      - GOSU_CHOWN=/home/node/.cache/gravv-${NODE_ENV} /home/node/.config/gravv-${NODE_ENV}
      - TZ=${TZ:-America/New_York}
    env_file:
      - .env.${NODE_ENV}
    hostname: api
    ports:
      - "${HUB_API_PUBLIC_PORT:-8080}:${HUB_API_PORT:-8080}"
    networks:
      - ${NODE_ENV}_network
    volumes:
      - .:/app
      - .config/gravv-${NODE_ENV}/:/home/node/.config/gravv-${NODE_ENV}/
      - gravv-hub-${NODE_ENV}:/home/node/.cache/gravv-${NODE_ENV}/
    depends_on:
      postgres:
        condition: service_healthy
      kafka: # CI REMOVE
        condition: service_healthy # CI REMOVE
    profiles: [development, production]

  # inbox can be run on peer nodes or the hub
  inbox:
    container_name: inbox-${NODE_ENV}
    build:
      context: ./
      dockerfile: ./docker/media/Dockerfile
      target: ${BUILD_ENV:-production}
    image: registry.gitlab.com/far-world-labs/gravv/inbox:latest
    command: npm run inbox:${NODE_ENV}
    environment:
      - GOSU_USER=${UID:-1000}:${GID:-1000}
      - GOSU_CHOWN=/home/node/.cache/gravv-${NODE_ENV} /home/node/.config/gravv-${NODE_ENV} ${WORKER_INBOX_01:-/inbox-01} ${WORKER_INBOX_02:-/inbox-02} ${WORKER_INBOX_03:-/inbox-03} ${HUB_INBOX_01:-/hub-inbox-01} ${HUB_INBOX_02:-/hub-inbox-02} ${HUB_INBOX_03:-/hub-inbox-03}
      - TZ=${TZ:-America/New_York}
    env_file:
      - .env.${NODE_ENV}
    networks:
      - ${NODE_ENV}_network
    volumes:
      - .:/app
      - .config/gravv-${NODE_ENV}/:/home/node/.config/gravv-${NODE_ENV}/
      - gravv-common-${NODE_ENV}:/home/node/.cache/gravv-${NODE_ENV}/
      - ${WORKER_INBOX_01:-./inbox-01}:${WORKER_INBOX_01:-/inbox-01}
      - ${WORKER_INBOX_02:-./inbox-02}:${WORKER_INBOX_02:-/inbox-02}
      - ${WORKER_INBOX_03:-./inbox-03}:${WORKER_INBOX_03:-/inbox-03}
      - ${HUB_INBOX_01:-./hub-inbox-01}:${HUB_INBOX_01:-/hub-inbox-01}
      - ${HUB_INBOX_02:-./hub-inbox-02}:${HUB_INBOX_02:-/hub-inbox-02}
      - ${HUB_INBOX_03:-./hub-inbox-03}:${HUB_INBOX_03:-/hub-inbox-03}
      - /etc/timezone:/etc/timezone:ro # CI REMOVE
      - /etc/localtime:/etc/localtime:ro # CI REMOVE
    depends_on: # CI REMOVE
      kafka: # CI REMOVE
        condition: service_healthy # CI REMOVE
    profiles: [development, test]

  postgres:
    image: postgres:16
    container_name: postgres-${NODE_ENV}
    restart: unless-stopped
    env_file:
      - .env.${NODE_ENV}
    hostname: postgres
    ports:
      - "${POSTGRES_PUBLIC_PORT:-5432}:${POSTGRES_PORT:-5432}"
    networks:
      - ${NODE_ENV}_network
    volumes:
      - gravv-postgres-${NODE_ENV}:/var/lib/postgresql/data
      # init script creates the initial database on first run
      - ./scripts/init.sql:/docker-entrypoint-initdb.d/init.sql
      - /etc/timezone:/etc/timezone:ro # CI REMOVE
      - /etc/localtime:/etc/localtime:ro # CI REMOVE
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U admin -d media"]
      interval: 5s
      timeout: 10s
      retries: 5
      start_period: 30s
    profiles: [test, development, production]

  kafka:
    image: bitnami/kafka:3.6.0
    container_name: kafka-${NODE_ENV}
    restart: unless-stopped
    env_file:
      - .env.${NODE_ENV}
    hostname: kafka
    ports:
      - "${KAFKA_PUBLIC_PORT:-40092}:${KAFKA_PORT:-9092}" # External port mapped to the internal Kafka broker listener
    environment:
      ALLOW_PLAINTEXT_LISTENER: "yes"
      KAFKA_BROKER_ID: "1"
      KAFKA_CFG_ADVERTISED_LISTENERS: PLAINTEXT://kafka:${KAFKA_PORT:-9092}
      KAFKA_CFG_CONTROLLER_LISTENER_NAMES: CONTROLLER
      KAFKA_CFG_CONTROLLER_QUORUM_VOTERS: 1@localhost:2181
      KAFKA_CFG_LISTENERS: PLAINTEXT://:${KAFKA_PORT:-9092},CONTROLLER://:2181
      KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP: CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT
      KAFKA_CFG_NODE_ID: "1"
      KAFKA_CFG_PROCESS_ROLES: broker,controller
      KAFKA_ENABLE_KRAFT: "yes"
      KAFKA_KRAFT_CLUSTER_ID: Kmp-xkTnSf-WWXhWmiorDg
    networks:
      - ${NODE_ENV}_network
    volumes:
      - gravv-kafka-${NODE_ENV}:/bitnami/kafka
      - /etc/timezone:/etc/timezone:ro # CI REMOVE
      - /etc/localtime:/etc/localtime:ro # CI REMOVE
    healthcheck:
      test: ["CMD-SHELL", "/opt/bitnami/kafka/bin/kafka-broker-api-versions.sh --bootstrap-server localhost:${KAFKA_PORT:-9092} > /dev/null 2>&1"]
      interval: 5s
      timeout: 5s
      retries: 10
      start_period: 60s
    profiles: [test, development, production]

  kafka-ui:
    image: provectuslabs/kafka-ui:latest
    container_name: kafka-ui-${NODE_ENV}
    restart: unless-stopped
    env_file:
      - .env.${NODE_ENV}
    ports:
      - "${KAFKA_UI_PORT}:8080"
    environment:
      KAFKA_CLUSTERS_0_NAME: local
      KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS: kafka:${KAFKA_PORT:-9092}  # Use internal Docker network port for Kafka
      DYNAMIC_CONFIG_ENABLED: 'true'
    networks:
      - ${NODE_ENV}_network
    profiles: [development, production]

networks:
  test_network:
  development_network:
  production_network:

volumes:
  gravv-hub-test:
    driver: local

  gravv-common-test:
    driver: local

  gravv-postgres-test:
    driver: local

  gravv-kafka-test:
    driver: local

  gravv-hub-development:
    driver: local

  gravv-common-development:
    driver: local

  gravv-postgres-development:
    driver: local

  gravv-kafka-development:
    driver: local

  gravv-hub-production:
    driver: nfs # CI REMOVE
    driver_opts: # CI REMOVE
      type: nfs # CI REMOVE
      o: addr=${NFS_HOST},nolock,soft,rw,timeo=20,nfsvers=4 # CI REMOVE
      device: "${NFS_HOST}:${NFS_HUB_PATH}" # CI REMOVE

  gravv-postgres-production:
    driver: nfs # CI REMOVE
    driver_opts: # CI REMOVE
      type: nfs # CI REMOVE
      o: addr=${NFS_HOST},nolock,soft,rw,timeo=20,nfsvers=4 # CI REMOVE
      device: "${NFS_HOST}:${NFS_POSTGRES_PATH}" # CI REMOVE

  gravv-kafka-production:
    driver: nfs # CI REMOVE
    driver_opts: # CI REMOVE
      type: nfs # CI REMOVE
      o: addr=${NFS_HOST},nolock,soft,rw,timeo=20,nfsvers=4 # CI REMOVE
      device: "${NFS_HOST}:${NFS_KAFKA_PATH}" # CI REMOVE
