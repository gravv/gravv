Gravv is a small distributed system for capturing, analyzing, and transforming media, designed for developers who want more control over images and videos across an increasing number of devices. It operates locally for optimal speed, privacy, and control.

Gravv nodes operate on your devices, in whatever folders you specify, and can be responsible for capturing, processing, or both. The system will then centrally store your media to a designated hub server. Gravv then redistributes everything as compressed files so every device has a local copy at a fraction of the size.

# System Overview

# General Setup

The Hub API, Kafka, and Postgres must be started before scanners and other workers.

```bash
npx dotenv -e .env.development -- docker compose --profile development up
```

# Running Scanners

Any number of machines can be used to process media. The worker included in this repository is a simple scanner used for ingestion and metadata capture. Other workers might perform machine learning operations or other tasks. The system publishes file processing events to a Kafka topic, that your programs can consume from.

```bash
node ./src/apps/inbox/index.js
```

## Systemd Setup

There are sample systemd service definitions in src/systemd. Customize those as necessary for running the hub on your server and the worker on whatever computers you decide to use for heavier processing.

The instructions here assume an XDG-style directory structure on Linux. Most locations can be specified via CLI or configuration files.

## Hub Configuration

The hub can be configured by placing a `config.json` in `/.config/.gravv-development/`. See the `./src/constants/` directory for other ways to configure:

```json
{
  "dryRun": false,

  "hubInboxes": ["/gravv/test/media/"],
}
```

The hub and the workers can watch folders on the filesystem for changes. Unlike workers, the hub does not process files to extract metadata but relies on clients to download it.

Some devices don't support Docker and therefore can't run these workers. In these cases it's possible to sync files to the hub via FTP using an app like FolderSync.

The hub performs various setup and cleanup operations when it's started. Kafka topics are created if they do not already exist.

### kafka topics
Kafka topics are the means by which downstream services listen as files are processed. Topics are suffixed with the app version.

```
media-events-v0.0.1
```

`media-events` is an event source configured for infinite retention, so dependent services can use a single Kafka consumer to loop over all processed media and then stay active for anything new that arrives.

# Worker Setup

Without Docker, a worker will need the install Exiftool, Imagemagick, and Ffmpeg on the machines that will be used as workers.

## Worker configuration

Clients can be configured by placing a `config.json` in `~/.config/gravv-development/`:
```
{
  "dryRun": false,

  "inboxes": ["/gravv/test/media/"],
  "hubHost": "gravv-hub",
}
```

# Storage

Files are stored as filenames matching their sha256sum hash and partitioned into folders to support storing millions of files.

## Firewall settings
sudo ufw allow 8080 # API
sudo ufw allow 9092 # Kafka

# Database

Run migrations:
```
. .env && NODE_ENV=test npx knex migrate:latest
```

# Tests

Each Gravv node type uses different OS dependencies, but the testing is performed in a single Docker container for convenience. This is accomplished by running a shared "common" container that includes all the dependencies required by any node type.

```bash
npx dotenv -e .env.test -- docker compose --profile test run inbox npm run test
```

## Additional Notes
Gravv isn't just intended for file sharing. The architecture supports building arbitrarily complex processing pipelines of operations. For each new specialization, a new worker can be created and installed to process media as it comes in. The API stores metadata as an aggregate that many cooperating workers can each manage part of.

Note that files in ingestion folders will be removed after they are copied to the hub. This is by design, as the system aims to unburden devices that capture media and offload responsibility for processing media. This solves the problem of devices running out of space, and files being split up between your devices. Gravv centralizes media and gathers metadata to support various use cases. In this way Gravv intends to support collaborative archival, cataloging, search, and analysis.

In specializing the machines on your network you initially decide which machines are used for storage, which are used for capture, and which are used for processing. The architecture enables minimal disk usage and compute on capture nodes, minimal disk usage on the processing nodes, and minimal computation on the hub server. The hub server should be large enough to store media collected across your devices. A compressed version of media can be redistributed back to devices if desired. This typically requires 10-20GB per TB of stored media depending on settings, and is designed to store an entire media collection on an average smartphone or laptop.

Note that this architecture is new and remains experimental. There are several unsolved problems I'm still working through, and there isn't much of an interface for the files once they've been captured. Do not use Gravv without first backing up your media.

Gravv works well atop a base ZFS filesystem. This enables useful features like replication and snapshots.
