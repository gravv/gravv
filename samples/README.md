# Sample Images

Currently there are only a few example images used for testing.

When the time comes to scale to more devices, take a look at the following collections:

https://exiftool.org/sample_images.html | Phil Harvey's collection from over 7000 camera models

https://github.com/ianare/exif-samples
