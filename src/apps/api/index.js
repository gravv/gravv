import fs from 'fs';
import https from 'https';

import {
  makeClient,
} from '../../common/kafka/index.js';
import {
  optUsage
} from '../../constants/core/index.js';
import {
  hubPort,
  apiSSLKeyPath,
  apiSSLCertPath,
} from '../../constants/core/index.js';
import {
  welcome
} from '../../controllers/system/index.js';
import ExpressApi from '../../services/express-api/index.js';
import log from '../../services/log/index.js';

const kafkaClient = makeClient({
  retry: {
    // for broker connection, re-elections, ...
    retries: 10,
  },
});

await new Promise(resolve => {
  const admin = kafkaClient.admin();
  admin.connect();
  admin.on(admin.events.CONNECT, resolve);
});

const run = async () => {
  if (optUsage) {
    log.error('Usage: node hub.js');
    process.exit(0);
  }

  log.info('Server starting [started]', {
    NODE_ENV: process.env.NODE_ENV,
    NODE_VERSION: process.env.NODE_VERSION,
  });

  const api = new ExpressApi({
    kafkaClient
  });

  const app = await api.build({ setup: true });

  const options = {
    key: fs.readFileSync(apiSSLKeyPath),
    cert: fs.readFileSync(apiSSLCertPath)
  };

  const server = https
	.createServer(options, app);
  server
    .listen(hubPort, welcome(server));
};

// TODO network check
// TODO onShutdown

run();
