import express from 'express';

import methodOverride from '../../middleware/method-override/index.js';
import routesTopLevel from '../../routes/api/index.js';

export default async ({
  routes = routesTopLevel,
}) => {
  const app = express();

  app.use(methodOverride);
  app.use(express.json());

  routes(app);

  return app;
}
