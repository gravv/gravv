/**
 * Tests the media and media metadata API as an HTTP client.
 * It ensures that various operations such as retrieving, adding, and deleting
 * media metadata work as expected,
 * and that the API endpoints handle different scenarios correctly.
 */

import express from 'express';
import { Factory } from 'fishery';
import supertest from 'supertest';
import { afterAll, beforeEach, describe, expect, it, vi } from 'vitest';

import router from '../../routes/api/index.js';
import producerService from '../../services/kafka-producer/index.js';
import logService from '../../services/log/index.js';

import appCreate from './init.js';

const getMedia = (app, hash) => {
  const result = supertest(app)
    .get(`/api/media/${hash}`)
    .send();
  return result;
};

const addMedia = (app, hash, media) => {
  const result = supertest(app)
    .patch(`/api/media/${hash}`)
    .send(media);
  return result;
};

const removeMedia = (app, hash, { hardRemove }={}) => {
  const result = supertest(app)
    .delete(`/api/media/${hash}${hardRemove ? '?hardRemove=true': ''}`)
    .send();
  return result;
};

const queryMedia = (app, query={}) => {
  const result = supertest(app)
    .post('/api/media/search')
    .set('x-http-method-override', 'QUERY')
    .send(query);
  return result;
};

const getRobots = (app) => {
  const result = supertest(app)
    .get('/robots.txt')
    .send();
  return result;
};

const getStatus = (app) => {
  const result = supertest(app)
    .get('/api/status')
    .send();
  return result;
};

let app;

vi.spyOn(logService, 'info').mockReturnValue(vi.fn());
vi.spyOn(logService, 'error').mockReturnValue(vi.fn());
vi.spyOn(logService, 'debug').mockReturnValue(vi.fn());

vi.spyOn(producerService, 'getInstance')
  .mockImplementation(async () => {
    return {
      connect: vi.fn(() => Promise.resolve()),
      send: vi.fn(() => Promise.resolve()),
      on: vi.fn(),
    };
  });

beforeEach(async () => {
  vi.clearAllMocks();

  app = await appCreate({
    router: express.Router().use('/', router)
  });

  await removeMedia(app, '1'.padStart(64, '0'), { hardRemove: true });
  await removeMedia(app, '2'.padStart(64, '0'), { hardRemove: true });
  await removeMedia(app, '3'.padStart(64, '0'), { hardRemove: true });
});

afterAll(async () => {
  await removeMedia(app, '1'.padStart(64, '0'), { hardRemove: true });
  await removeMedia(app, '2'.padStart(64, '0'), { hardRemove: true });
  await removeMedia(app, '3'.padStart(64, '0'), { hardRemove: true });
});

// Define the metadata factory using fishery
const mediaFactory = Factory.define(({ sequence, transientParams }) => {
  const { withStat, withIdentity, id } = transientParams;

  const now = new Date().toISOString();
  const media = {
    filename: `test-file-${id ?? sequence}.jpg`,
    hash: `${id ?? sequence}`.padStart(64, '0'),
    size: 1024,
    type: 'jpg',
    createdAt: now,
    modifiedAt: now
  };

  if (withStat) {
    media.stat = {
      size: 1024,
      atime: now,
      mtime: now,
      ctime: now,
      birthtime: now,
      gid: 1000,
      isDirectory: false,
      isFile: true,
      isSymbolicLink: false,
      mode: 0o644,
      uid: 1000
    };
  }

  if (withIdentity) {
    media.identity = {
      ext: 'jpeg',
      file: `/path/to/file_${id ?? sequence}.jpeg`,
      sha256: 'dummyhashvalue',
      signature: 'dummysignature',
      machineId: 'dummyid'
    };
  }

  return media;
});

const listExamples = [
  {
    name: 'Basic usage',
    inputs: {
      hash: '1'.padStart(64, '0'),
      data: mediaFactory.build({}, {
	transient: {
	  id: 1,
	  withStat: true,
	  withIdentity: true
	}
      })
    },
    want: { status: 200 },
  },
];

describe('Metadata API - List', () => {
  listExamples.forEach((example) => {
    it(example.name, async () => {
      await addMedia(app, example.inputs.hash, example.inputs.data)
	.expect(200);

      const hasResult = queryMedia(app);

      const result = await hasResult;

      const data = JSON.parse(result.text);
      expect(data.status, result.message ?? result.text).toEqual(example.want.status);
      expect(+data.pagination.total).gt(0);
    });
  });
});

const retrieveExamples = [
  {
    name: 'Basic usage',
    inputs: {
      hash: '1'.padStart(64, '0'),
      data: mediaFactory.build({}, {
	transient: {
	  id: 1,
	  withStat: true,
	  withIdentity: true
	}
      })
    },
    want: {
      status: 200,
    },
  },
  {
    name: 'Basic error',
    inputs: {
      hash: '10'.padStart(64, '0'),
      data: mediaFactory.build({}, {
	transient: {
	  id: 2, // not equal
	  withStat: true,
	  withIdentity: true
	}
      })
    },
    want: { status: 404 }
  }
];

describe('Metadata API - Retrieve', () => {
  retrieveExamples.forEach((example) => {
    it(example.name, async () => {
      await addMedia(app, example.inputs.hash, example.inputs.data)
	.expect(200);

      const hasResult = getMedia(app, example.inputs.hash)
        .expect(example.want.status);

      const result = await hasResult;

      const data = JSON.parse(result.text);
      expect(data.status, result.message ?? result.text)
	.toEqual(example.want.status);

      if (example.want.status === 200) {
        expect(result.body.data.filename).toEqual(example.inputs.data.filename);
        expect(result.body.data.hash).toEqual(example.inputs.hash);
      }
    });
  });
});

const addExamples = [
  {
    name: 'Basic usage',
    inputs: {
      hash: '2'.padStart(64, '0'),
      media: mediaFactory.build({}, {
	transient: {
	  id: 2,
	  withStat: true,
	  withIdentity: true
	}
      })
    },
    want: { status: 200 },
  },
];

describe('Metadata API - Add', () => {
  addExamples.forEach((example) => {
    it(example.name, async () => {
      await addMedia(app, example.inputs.hash, example.inputs.media)
	.expect(example.want.status);
    });
  });
});

const removeExamples = [
  {
    name: 'Basic usage',
    inputs: {
      hash: '3'.padStart(64, '0'),
      id: 3,
    },
    want: { status: 200 },
  },
];

describe('Metadata API - Remove', () => {
  removeExamples.forEach((example) => {
    it(example.name, async () => {
      // First, add the media to ensure it exists
      const media = mediaFactory.build({}, {
	transient: {
	  id: example.inputs.id,
	  withStat: true,
	  withIdentity: true
	}
      });
      await addMedia(app, example.inputs.hash, media)
	.expect(200);

      // Now, remove the media
      await removeMedia(app, example.inputs.hash, { hardRemove: true })
	.expect(example.want.status);

      // Finally, try to get the media and expect a 404
      await getMedia(app, example.inputs.hash)
	.expect(404);
    });
  });
});

describe('System API', () => {
  it('Disallows robots', async () => {
    await getRobots(app).expect(200);
    await getStatus(app).expect(200);
  });
});
