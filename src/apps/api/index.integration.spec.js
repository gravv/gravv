import { describe, it, expect } from 'vitest';

import { networkCheck } from '../../common/network/index.js';
import dbService from '../../services/postgres/index.js';

describe('Postgres', () => {
  it('Verify the instance is up', async () => {
    try {
      // Query to check if the PostgreSQL instance is up
      const db = dbService.getInstance();
      const result = await db.raw('SELECT 1+1 AS result');
      expect(result).toBeDefined();
      expect(result.rows[0].result).toBe(2);
    } catch (error) {
      throw new Error(`PostgreSQL instance is down: ${error.message}`);
    }
  });

  it('Verify media table contents look accurate', async () => {
    try {
      const db = dbService.getInstance();
      // Query to check if the files table is not empty
      const result = await db('media').count('id as count');
      const count = parseInt(result[0].count, 10);

      const isTest = process.env.NODE_ENV === 'test';

      if (isTest) {
	expect(count).toEqual(0);
      } else {
	expect(count).toBeGreaterThan(0);
      }
    } catch (error) {
      throw new Error(`Error checking files table: ${error.message}`);
    }
  });
});

describe('Network', () => {
  it('Verify the network is up', async () => {
    await networkCheck();
  });
});
