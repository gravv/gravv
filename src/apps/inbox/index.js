import {
  removeEmptyDirectories,
  createDir,
} from '../../common/files/index.js';
import {
  makeClient,
  setupTopics,
} from '../../common/kafka/index.js';
import onShutdown from '../../common/lib/shutdown/index.js';
import {
  networkCheck,
  serverCheck,
} from '../../common/network/index.js';
import retryWith from '../../common/retry/index.js';
import {
  inboxes,
  networkCheckInterval,
  storage,
} from '../../constants/core/index.js';
import {
  mediaEventConfig,
} from '../../constants/kafka-topics/index.js';
import {
  addFile,
} from '../../controllers/inbox/index.js';
import InboxWorker from '../../services/inbox-worker/index.js';

await setupTopics([
  mediaEventConfig,
], { client: makeClient() });

await createDir(storage.stageTemp);

const worker = new InboxWorker(inboxes);

await worker.start();

worker.onFile(retryWith(addFile));

onShutdown(async () => {
  await worker.cleanup();
  await removeEmptyDirectories(storage.stageTemp);
});

setInterval(() => {
  networkCheck();
  serverCheck();
}, networkCheckInterval);
