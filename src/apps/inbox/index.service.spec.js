/**
 * Tests file processing, which is invoked through filesystem watchers.
 * Tests validate that metadata is extracted and files are sent to the API.
 */
import fsCb from 'fs';
import path from 'path';
import { Readable } from 'stream';

import axios from 'axios';
import { afterAll, beforeEach, describe, expect, it, vi } from 'vitest';

import * as commonFiles from '../../common/files/index.js';
import * as constants from '../../constants/core/index.js';
import { addFile, scanMetadata } from '../../controllers/inbox/index.js';
import MediaClient from '../../repositories/media-client/index.js';
import DeepScanService from '../../services/deep-scan/index.js';
import producerService from '../../services/kafka-producer/index.js';
import logs from '../../services/log/index.js';

// BE VERY CAREFUL WITH THESE PATHS. rm -rf is used.
const TEST_ROOT = path.join(process.cwd(), '.test', 'inbox-service');
const FILES_ROOT = path.join(process.cwd(), '.data');
const CACHE_ROOT = path.join(process.cwd(), '.cache');

const TEST_INBOX = path.join(TEST_ROOT, 'inbox1');
const GOOD_FILES_PATH = path.join(process.cwd(), './samples/test-good');
const IGNORED_PATH = path.join(process.cwd(), './samples/samples-ignored');

// Make the constants mutable
vi.mock('../../constants/core/index.js', async (importOriginal) => ({
  ...(await importOriginal()),
}));

const fs = fsCb.promises;

const storageInitial = constants.storage;
const hubStorageInitial = constants.hubStorage;

const makeLocalAxios = () => ({
  get: vi.fn(),
  put: vi.fn(),
  post: vi.fn(),
  patch: vi.fn(),
  delete: vi.fn(),
});

beforeEach(async () => {
  vi.clearAllMocks();

  constants.storage = {
    ...storageInitial,
    stageTemp: '.cache',
  };

  constants.hubStorage = {
    ...hubStorageInitial,
    files: '.data',
  };

  vi.spyOn(axios, 'create').mockReturnValue(vi.fn());

  vi.spyOn(logs, 'info').mockReturnValue(vi.fn());
  vi.spyOn(logs, 'error').mockReturnValue(vi.fn());
  vi.spyOn(logs, 'debug').mockReturnValue(vi.fn());

  vi.spyOn(producerService, 'getInstance')
    .mockImplementation(async () => {
      return {
	connect: vi.fn(() => Promise.resolve()),
	send: vi.fn(() => Promise.resolve()),
	on: vi.fn(),
      };
    });

  vi.spyOn(MediaClient.prototype, 'download')
    .mockResolvedValue(Promise.resolve(fsCb.createReadStream(
      path.join(GOOD_FILES_PATH, 'good.jpg')
    )));

  vi.spyOn(fsCb.promises, 'unlink').mockResolvedValue();

  await fs.mkdir(TEST_INBOX, { recursive: true });
  await fs.mkdir(FILES_ROOT, { recursive: true });
  await fs.mkdir(CACHE_ROOT, { recursive: true });
});

afterAll(async () => {
  await fs.rm(TEST_ROOT, { recursive: true, force: true });
  await fs.rm(FILES_ROOT, { recursive: true, force: true });
  await fs.rm(CACHE_ROOT, { recursive: true, force: true });
});

describe('File Processing Integration Tests', () => {
  describe('Deep scan file', () => {
    const testCases = [
      {
        description: 'Deep scan local image (hub or worker captured)',
        input: { file: path.join(GOOD_FILES_PATH, 'good.jpg') },
        want: {
	  cleanedFiles: true,
	  results: {},
        },
      },
      {
        description: 'Deep scan local video (hub or worker captured)',
        input: { file: path.join(GOOD_FILES_PATH, 'good.mp4') },
        want: {
	  cleanedFiles: true,
	  results: {},
        },
      },
      {
        description: 'Skip deep scan if worker lacks capabilities',
        input: { file: 'testFile', blockDeepScan: true },
        want: {
          downloadCalled: false,
          mediaSetCalled: false,
        },
      },
      {
        description: 'Deep scan error when copying',
        input: { file: path.join(GOOD_FILES_PATH, 'good.jpg'), triggerCopyError: true },
        want: {
          downloadCalled: false,
          mediaSetCalled: false,
        },
      },
    ];
    testCases.forEach(({ description, input, want }) => {
      it(description, async () => {
	if (input.blockDeepScan) {
	  vi.spyOn(DeepScanService.prototype, 'isValidWorker').mockReturnValue(false);
	}

	const localAxios = makeLocalAxios();
	axios.create.mockReturnValue(localAxios);

	if (input.file && !input.blockDeepScan) {
	  await fs.copyFile(input.file, path.join(TEST_INBOX, path.basename(input.file)));
	}

        await scanMetadata(input.remoteHash, { file: input.file });

        // Check axios calls based on 'want' conditions
        if (want.downloadCalled) {
          expect(MediaClient.prototype.download).toHaveBeenCalledWith(
            input.remoteHash,
	  );
        }

        // Verify staging files were removed correctly
        if (want.cleanedFiles) {
          expect(fs.unlink).toHaveBeenCalled();
        }
      });
    });
  });
  describe('Quick scan file', () => {
    const testCases = [
      {
        description: 'Quick scan local file (hub or worker captured)',
        input: {
	  file: path.join(GOOD_FILES_PATH, 'good.jpg'),
	},
        want: {
	  uploadCalled: true,
	  mediaSetCalled: true,
	  removeFileCalled: true,
	  results: {},
        },
      },
      {
	description: 'Quick scan skips invalid files',
	input: {
	  file: path.join(IGNORED_PATH, 'jar.jar'),
	},
	want: {
	  uploadCalled: false,
	}
      },
    ];
    testCases.forEach(({ description, input, want }) => {
      it(description, async () => {
	vi.spyOn(DeepScanService.prototype, 'isValidWorker').mockResolvedValue(false);
	vi.spyOn(commonFiles, 'removeFile').mockImplementation();

	const localAxios = makeLocalAxios();
	localAxios.post.mockReturnValue({ data: { data: { hash: '123'.padStart(64, '0') } } });
	localAxios.get.mockReturnValue({ data: { data: { hash: '123'.padStart(64, '0') } } });
	axios.create.mockReturnValue(localAxios);

        // Copy the test file to the inbox
        const testFile = path.join(TEST_INBOX, path.basename(input.file));

	await fs.copyFile(input.file, testFile);

        await addFile(testFile);

        // Check axios calls based on 'want' conditions
        if (want.uploadCalled) {
          expect(localAxios.post).toHaveBeenCalledWith(
	    '/upload',
            expect.any(Readable),
            expect.any(Object), // internally supplied config
	  );
        }

        if (want.mediaSetCalled) {
          expect(localAxios.patch).toHaveBeenCalledWith(
	    expect.stringMatching(/[a-zA-Z0-9]{64}/),
	    expect.any(Object), // body
	  );
        }

        // Check if removeFile was called when expected
        if (want.removeFileCalled) {
          expect(commonFiles.removeFile).toHaveBeenCalledWith(
	    testFile,
	    expect.any(Object)
	  );
        }
      });
    });
  });
});
