export default class StatData {
  constructor({ ...data }={}) {
    this.atime = new Date(data.atime);
    this.birthtime = data.birthtime !== 0 ? new Date(data.birthtime) : undefined;
    this.ctime = new Date(data.ctime);
    this.gid = data.gid;
    this.isDirectory = data.isDirectory;
    this.isFile = data.isFile;
    this.isSymbolicLink = data.isSymbolicLink;
    this.mode = data.mode;
    this.mtime = new Date(data.mtime);
    this.size = data.size;
    this.uid = data.uid;
  }

  toJSON() {
    return {
      atime: this.atime.toISOString(),
      birthtime: this.birthtime.toISOString(),
      ctime: this.ctime.toISOString(),
      gid: this.gid.toString(),
      isDirectory: this.isDirectory,
      isFile: this.isFile,
      isSymbolicLink: this.isSymbolicLink,
      mode: this.mode.toString(),
      mtime: this.mtime.toISOString(),
      size: this.size,
      uid: this.uid.toString(),
    };
  }
}
