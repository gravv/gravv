import {
  ffmpegProperties,
} from '../../constants/files/index.js';

const filterProperties = (data, allowedProperties) => {
  const filteredData = {};
  Object.keys(data).forEach(key => {
    if (allowedProperties.includes(key)) {
      filteredData[key] = data[key];
    }
  });
  return filteredData;
};

export default class FfmpegData {
  constructor({ ...data } = {}) {
    Object.assign(this, filterProperties(data, ffmpegProperties));
  }

  toJSON() {
    return filterProperties(this, ffmpegProperties);
  }
}
