import {
  imagemagickProperties,
} from '../../constants/files/index.js';

const filterProperties = (data, allowedProperties) => {
  const filteredData = {};
  Object.keys(data).forEach(key => {
    if (allowedProperties.includes(key)) {
      filteredData[key] = data[key];
    }
  });
  return filteredData;
};

export default class ImagemagickData {
  constructor({ ...data } = {}) {
    Object.assign(this, filterProperties(data, imagemagickProperties));
  }

  toJSON() {
    return filterProperties(this, imagemagickProperties);
  }
}
