export default class IdentityData {
  constructor(data) {
    Object.assign(this, data);
  }

  toJSON() {
    return {
      ext: this.ext,
      file: this.file,
      sha256: this.sha256,
      signature: this.signature,
      machineId: this.machineId,
      machineTZ: this.machineTZ,
    };
  }
}
