import {
  mediaEventConfig
} from '../../constants/kafka-topics/index.js';
import Media from '../../entities/media/index.js';
import { NotFound } from '../../errors/index.js';
import MediaRepository from '../../repositories/media/index.js';
import kafkaProducerService from '../../services/kafka-producer/index.js';

export const upload = async (req) => {
  const mediaRepository = new MediaRepository();
  const hashOverride = req.headers['media-hash'];
  const nameHeader = req.headers['media-name'];

  const name = `${nameHeader ? '-' + nameHeader : ''}`;

  const hash = await mediaRepository.upload(req, { hashOverride, name });

  return { status: 201, data: { hash } };
};

export const download = async (req, res) => {
  const { hash } = req.params;
  const { tags, size } = req.query;
  const mediaRepository = new MediaRepository();
  const media = await mediaRepository.get(hash);
  if (!media) throw new NotFound();

  const requestedTags = tags ? tags.split(',') : [];
  const artifact = media.getArtifact(requestedTags, size || 'large');
  const name = artifact?.name;

  try {
    const downloadStream = mediaRepository.download(`${hash}${name ? '-' + name : ''}`);

    downloadStream.pipe(res);
  } catch (error) {
    if (error.code === 'ENOENT') {
      throw new NotFound();
    }
  }
};

export const get = async (req) => {
  const { hash } = req.params;
  const mediaRepository = new MediaRepository();
  const media = await mediaRepository.get(hash);
  if (!media) throw new NotFound();
  return { status: 200, data: media.toJSON() };
};

export const search = async (req) => {
  const mediaRepository = new MediaRepository();
  const result = await mediaRepository.search(req.body);

  return {
    status: 200,
    data: result.data.map(d => d.toJSON()),
    pagination: result.pagination
  };
};

export const patch = async (req) => {
  // TODO validate url hash and posted object agree
  const media = Media.fromJSON({
    ...req.body
  });

  const mediaRepository = new MediaRepository();
  await mediaRepository.patch(media);

  const mediaEventMessage = {
    key: req.body.hash,
    topic: mediaEventConfig.topic,
    messages: [{
      value: JSON.stringify({
	eventType: 'FileMetadataFound',
	timestamp: (new Date()).toISOString(),
	data: media.toJSON(),
      })
    }],
  };

  const producer = await kafkaProducerService.getInstance();
  await producer.send(mediaEventMessage);

  return { status: 200 };
};

export const remove = async (req) => {
  const { hardRemove } = req.query;
  const { hash } = req.params;
  const mediaRepository = new MediaRepository();
  const media = await mediaRepository.get(hash);
  await mediaRepository.remove(hash, { hardRemove });

  if (!media) {
    return { status: 200 };
  }

  const mediaEventMessage = {
    key: req.body.hash,
    topic: mediaEventConfig.topic,
    messages: [{
      value: JSON.stringify({
	eventType: 'FileRemoved',
	timestamp: (new Date()).toISOString(),
	data: media.toJSON(),
      })
    }],
  };

  const producer = await kafkaProducerService.getInstance();
  await producer.send(mediaEventMessage);

  return { status: 200 };
};
