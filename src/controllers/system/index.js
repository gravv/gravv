import os from 'os';

import logService from '../../services/log/index.js';

const logger = logService;

export const welcome = (server) => () => {
  logger.info(`Server starting [complete]: (host: ${server.address().address}:${server.address().port})`, {
    NODE_ENV: process.env.NODE_ENV,
    hostname: os.hostname(),
  });
};

export const getRobots = (req, res) => {
  const robotTxt = `User-agent: *
Disallow: /`;

  res.set('Content-Type', 'text/plain');
  res.send(robotTxt);
};

export const getStatus = (req, res) => {
  res.json({
    status: 200,
  });
};
