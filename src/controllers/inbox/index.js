import fsCb from 'fs';

import {
  exists,
  removeFile,
} from '../../common/files/index.js';
import {
  status
} from '../../common/lib/index.js';
import { validateFile } from '../../common/media/index.js';
import {
  hubUrl,
  machineId,
  storage,
} from '../../constants/core/index.js';
import {
  resizes,
} from '../../constants/files/index.js';
import Media from '../../entities/media/index.js';
import MediaClientRepository from '../../repositories/media-client/index.js';
import TempStorage from '../../repositories/temp-storage/index.js';
import DeepScanService from '../../services/deep-scan/index.js';
import log from '../../services/log/index.js';
import QuickScanService from '../../services/quick-scan/index.js';
import ResizeService from '../../services/resize/index.js';

const copyToTemp = async (file, { hash }) => {
  const store = new TempStorage();

  const copiedFile = store.makePath(storage.stageTemp, file ?? hash);
  const isExists = await exists(copiedFile);

  if (isExists) {
    return copiedFile;
  }

  let mediaInput;
  if (file) {
    mediaInput = fsCb.createReadStream(file);
  } else {
    const mediaClient = new MediaClientRepository(hubUrl);
    mediaInput = await mediaClient.download(hash);
  }

  return await store.copyFrom(mediaInput, storage.stageTemp, {
    name: file ?? hash,
  });
};

export const scanMetadata = async (hash, { file }={}) => {
  if (!(new DeepScanService()).isValidWorker()) {
    log.info(`Scan metadata ${status.skipped} (file: ${file}, hash: ${hash})`);
    return undefined;
  }

  let fileStored;
  try {
    fileStored = await copyToTemp(file, { hash });

    const deepScan = new DeepScanService();
    return await deepScan.scan(fileStored, {
      hash,
    });
  } catch (error) {
    throw error;
  } finally {
    await removeFile(fileStored, {
      reasonText: '',
      missingText: '',
      removeErrorText: ''
    });

    log.info(`Scan metadata ${status.complete} (file: ${file}, hash: ${hash})`);
  }
};

export const addFile = async (file) => {
  log.info(`Scan inbox file ${status.started} (file: ${file})`);

  const isValid = await validateFile(file);
  if (!isValid) {
    log.info(`Scan inbox file ${status.skipped}: unsupported (file: ${file})`);
    return;
  }

  const mediaClient = new MediaClientRepository(hubUrl);

  const hashResult = await mediaClient.upload(file);

  const hash = hashResult.data.data.hash;

  const quickScan = new QuickScanService();

  const mediaBase = await quickScan.scan(file, {
    hash,
    machineId
  });

  let fileTemp;
  let mediaDeep;
  let resizeResult;
  try {
    fileTemp = await copyToTemp(file, { hash });

    const deepScan = new DeepScanService();
    mediaDeep = await deepScan.scan(fileTemp, {
      hash,
    });

    const resize = new ResizeService();
    resizeResult = await resize.scan(mediaDeep, resizes);
  } finally {
    await removeFile(fileTemp, {
      reasonText: `Temporary file ${status.removed}. (file: ${fileTemp})`,
      missingText: `Attept to remove ${status.skipped} (file: ${fileTemp})`,
      removeErrorText: `Attempt to remove ${status.error} (file: ${fileTemp})`
    });
  }

  // Simultaneous uploads
  const allArtifactFiles = [...resizeResult.videos, ...resizeResult.images];
  const hasArtifactsUploaded = allArtifactFiles.map(async artifact => {
    return mediaClient.upload(
      artifact.path, {
	name: artifact.name,
	hash,
      }
    );
  });

  let mediaMerged = mediaBase;
  if (mediaDeep) {
    mediaMerged = Media.merge(mediaBase, mediaDeep);
  }

  resizeResult.videos.forEach(video => {
    mediaMerged.addArtifact({
      name: `base-${video.name}`,
      description: '',
      tags: ['role:base', `size:${video.name}`]
    });
  });

  resizeResult.images.forEach(image => {
    const type = resizeResult.videos.length ? 'role:cover' : 'role:base';
    mediaMerged.addArtifact({
      name: `${type === 'role:cover' ? 'cover-' : ''}${image.name}`,
      description: '',
      tags: [type, `size:${image.name}`],
    });
  });

  mediaMerged.addSnippet(resizeResult.crop);

  await Promise.all(hasArtifactsUploaded);

  await mediaClient.set(mediaMerged.toJSON({ collections: 'keyValue' }));

  await removeFile(file, {
    reasonText: `Scan inbox file ${status.removed}. File previously ingested (file: ${file})`,
    missingText: `Scan inbox file. Attempt to remove ${status.skipped}: File missing (file: ${file})`,
    removeErrorText: `Scan inbox file. Attempt to remove ${status.error} (file: ${file})`,
  });

  log.info(`Scan inbox file ${status.complete} (file: ${file}, hash: ${hash}, deepScan: true)`);
};
