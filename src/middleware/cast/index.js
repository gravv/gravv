import { object } from 'yup';

import ValidationError from '../../errors/validation/index.js';

export default (schema=object({})) => (req, res, next) => {
  try {
    // req.body = schema.cast(req.body, { stripUnknown: true });
    req.query = schema.cast(req.query, { stripUnknown: true });
    req.params = schema.cast(req.params, { stripUnknown: true });
    next();
  } catch (error) {
    throw new ValidationError('Casting error', error, { description: 'Error casting request data.' });
  }
};
