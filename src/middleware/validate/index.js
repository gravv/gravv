import { object } from 'yup';

import ValidationError from '../../errors/validation/index.js';

const validateWith = (schema=object({}).defined()) => async (req) => {
  try {
    await schema.validate(req.body, { abortEarly: false });
    await schema.validate(req.query, { abortEarly: false });
    await schema.validate(req.params, { abortEarly: false });
  } catch (error) {
    throw new ValidationError('Validation failed', { error });
  }
};

export default validateWith;
