import httpErrors from 'http-errors'

export default class NotFound extends httpErrors.NotFound {
  constructor(message, error, { description }={}) {
    super(message ?? error?.message ?? description ?? 'Not found.')
    this.code = 'not-found'
    this.description = description ?? 'Not found.'
    this.name = 'NotFoundError'
  }
}
