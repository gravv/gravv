import httpErrors from 'http-errors';

export default class ValidationError extends httpErrors.BadRequest {
  constructor(message, error, { description } = {}) {
    super(message ?? error?.message ?? description ?? 'Validation errors.');
    this.code = 'validation-error';
    this.description = description ?? 'Validation errors.';
    this.name = 'ValidationError';
  }
}
