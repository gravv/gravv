import { applyTemplate } from '../../common/lib/index.js';
import {
  appId as namespace,
  appVersion as version,
} from '../core/index.js';

const versionTemplate = '{name}--{namespace}-{version}';

const mediaEvent = applyTemplate({
  name: 'media-event',
  namespace,
  version,
}, versionTemplate);

export const mediaEventConfig = {
  topic: mediaEvent,
  numPartitions: 1,
  configEntries: [
    { name: 'retention.bytes', value: '-1' },
    { name: 'retention.ms', value: '-1' },
  ],
};
