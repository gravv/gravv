import fs from 'fs';

import minimist from 'minimist';

import {
  configDirEnvironment as env,
  resolve,
} from '../../common/lib/index.js';

const argv = minimist(process.argv);

const configDir = argv.c ?? `~/.config/gravv${env}/config.json`;

const config = JSON.parse(fs.readFileSync(resolve(configDir)));

export const ignorePatterns = config.ignorePatterns ? config.ignorePatterns : [];

export const resizes = {
  'video/mp4': {
    small: {
      name: 'SD',
      size: 640,
      crf: 18,
      preset: 'veryfast',
    },
    large: {
      name: 'FHD',
      size: 1920,
      crf: 18,
      preset: 'veryfast',
    }
  },
  'image/jpeg': {
    small: {
      name: 'small',
      size: 640,
      quality: 75,
      progressive: true,
      optimize: true,
    },
    large: {
      name: 'large',
      size: 1920,
      quality: 85,
      progressive: true,
      optimize: true,
    }
  },
  'image/png': {
    small: {
      name: 'small',
      size: 640,
      compressionLevel: 6,
      adaptiveFiltering: true,
    },
    large: {
      name: 'large',
      size: 1920,
      compressionLevel: 4,
      adaptiveFiltering: true,
    }
  }
};

export const targetVideoType = config.targetVideoType || 'mp4';

export const targetVideoCodec = config.targetVideoCodec || 'h264';

export const targetImageType = config.targetImageType || 'jpg';

export const exifProperties = config.exifProperties ? config.exifProperties : [
  'Aperture',
  'Author',
  'CreateDate',
  'DateTimeOriginal',
  'ExifVersion',
  'ExifVersion',
  'ExposureCompensation',
  'ExposureMode',
  'ExposureProgram',
  'FNumber',
  'FileAccessDate',
  'FileModifyDate',
  'FileName',
  'FileSize',
  'FileType',
  'FocalLength',
  'GPSAltitude',
  'GPSLatitude',
  'GPSLatitudeRef',
  'GPSLongitude',
  'GPSLongitudeRef',
  'ISO',
  'ImageHeight',
  'ImageSize',
  'ImageWidth',
  'Megapixels',
  'MeteringMode',
  'Model',
  'ModifyDate',
  'Orientation',
  'PDF Version',
  'Page Count',
  'ResolutionUnit',
  'ShutterSpeed',
  'SourceFile',
  'Title',
  'UserComment',
  'XResolution',
  'YResolution',
];

export const ffmpegProperties = config.ffmpegProperties ? config.ffmpegProperties : [
  'bitRate', // derived
  'codecName', // derived
  'colorSpace', // derived
  'duration', // derived
  'formatName', // derived
  'frameRate', // derived
  'height', // derived
  'nbStreams', // derived
  'probeScore', // derived
  'width', // derived
];

// Some properties sometimes return oddly large arrays:
// Colorspace, Gamma, Version, transparentColor (returns array of functions)
export const imagemagickProperties = config.imagemagickProperties ? config.imagemagickProperties : [
  'depth',
  'entropy', // derived
  'height', // derived
  'kurtosis', // derived
  'mean', // derived
  'Orientation',
  'Quality',
  'signature', // derived
  'skewness', // derived
  'width', // derived
];

export const statProperties = config.statProperties ? config.statProperties : [
  'atime',
  'birthtime',
  'ctime',
  'gid',
  'isDirectory', // derived
  'isFile', // derived
  'isSymbolicLink', // derived
  'mode',
  'mtime',
  'size',
  'uid',
];

export const frequentlyChangingProperties = [
  'exifFileAccessDate',
  'exifFileModifyDate',
  'exifModifyDate',
  'statAtime',
  'statBirthtime',
  'statCtime',
];

export const bufferTypesSupported = config.bufferTypesSupported ? config.bufferTypesSupported : [
  '3g2',
  '3gp',
  'ac3',
  'amr',
  'apng',
  'arw',
  'avi',
  'avif',
  'bmp',
  'cr2',
  'cr3',
  'dng',
  'eot',
  'epub',
  'f4a',
  'f4b',
  'f4p',
  'f4v',
  'flac',
  'flif',
  'gif',
  'icns',
  'ico',
  'jpg',
  'jxr',
  'm4a',
  'm4b',
  'm4p',
  'm4v',
  'mid',
  'mkv',
  'mobi',
  'mov',
  'mp1',
  'mp2',
  'mp3',
  'mp4',
  'mpg',
  'nef',
  'oga',
  'ogg',
  'ogm',
  'ogv',
  'opus',
  'orf',
  'otf',
  'pdf',
  'pgp',
  'png',
  'qcp',
  'raf',
  'rtf',
  'rw2',
  'shp',
  'tif',
  'ttf',
  'voc',
  'wav',
  'webm',
  'webp',
  'woff',
  'woff2',
];

export const bufferTypesImage = config.bufferTypesImage ? config.bufferTypesImage : [
  'apng',
  'arw',
  'bmp',
  'cr2',
  'cr3',
  'dng',
  'flif',
  'gif',
  'icns',
  'ico',
  'jpg',
  'jxr',
  'nef',
  'orf',
  'png',
  'raf',
  'rw2',
  'tif',
  'ttf',
  'voc',
  'webp',
];

export const bufferTypesAudio = config.bufferTypesAudio ? config.bufferTypesAudio : [
  'ac3',
  'amr',
  'flac',
  'm4a',
  'm4p',
  'mid',
  'mp1',
  'mp2',
  'mp3',
  'oga',
  'ogg',
  'ogm',
  'ogv',
  'opus',
  'voc',
  'wav',
];

export const bufferTypesVideo = config.bufferTypesVideo ? config.bufferTypesVideo : [
  '3g2',
  '3gp',
  'apng',
  'avi',
  'avif',
  'f4a',
  'f4b',
  'f4p',
  'f4v',
  'gif',
  'm4b',
  'm4v',
  'mkv',
  'mov',
  'mp4',
  'mpg',
  'webm',
];

// https://sharp.pixelplumbing.com/api-constructor
export const sharpTypes = [
  'jpg',
  'png',
  'webp',
  'gif',
  'svg',
  'tiff',
  'raw',
];

export const ffmpegCodecs = [
  'gif',
  'h264',
  'hevc',
  'mjpeg',
  'png',
];

export const ffmpegFormats = [
  'gif',
  'jpeg_pipe', // jpeg image
  'mov,mp4,m4a,3gp,3g2,mj2', // Quicktime
  'png_pipe', // png image
  'nsv',
];

export const exifExposureModes = [
  'Auto',
  'Manual',
];

export const exifMeteringModes = [
  'Average',
  'Center-weighted average',
  'ESP',
  'Evaluative',
  'Multi-segment',
  'Unknown',
];

export const imagemagickOrientationEnum = [
  'BottomRight',
  'LeftBottom',
  'RightTop',
  'TopLeft',
];

export const exifGPSEnum = [
  'E',
  'N',
  'S',
  'W',
];
