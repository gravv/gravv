import fs from 'fs';

import Kafka from 'kafkajs';
import minimist from 'minimist';

import {
  resolve,
  resolveRoot,
  falsyValues,
  createJSONFileIfNotExists,
  configDirEnvironment as env,
} from '../../common/lib/index.js';

const argv = minimist(process.argv);

export const isDocker = (
  !!process.env.GOSU_USER ||
  !!process.env.GOSU_CHOWN ||
  !!process.env.IS_DOCKER
);
export const isTest = process.env.NODE_ENV === 'test';
export const isDevelopment = process.env.NODE_ENV === 'development';
export const isProduction = process.env.NODE_ENV === 'production';

// change these values in ~/.config/gravv-${NODE_ENV}/config.json
const configStarter = {
  inboxes: [],
};

export const configDir = argv.c ?? `~/.config/gravv${env}/config.json`;
export const config = await createJSONFileIfNotExists(resolve(configDir), configStarter);

export const dryRun = argv.n ?? config.dryRun ?? false;

export let machineId = process.env.MACHINE_ID ?? config.machineId;
if (!isDocker) {
  machineId = fs.readFileSync('/etc/machine-id', 'utf8')
    .replace(/[^a-f0-9]/g, '');
}

export const apiSSLKeyPath = config.apiSSLKeyPath ?? resolveRoot('./local/localhost.key');
export const apiSSLCertPath = config.apiSSLCertPath ?? resolveRoot('./local/localhost.cert');

// corresponds to the cluster, used to separate kafka topics per environment
export const appId = config.appId ?? 'main';

export const appVersion = JSON.parse(fs.readFileSync(`${resolveRoot('./package.json')}`)).version;

export const inboxes = config.inboxes ? config.inboxes.map(d => resolve(d)) : [];

export const storage = {
  stageTemp: resolve(config?.storage?.stageTemp ?? `~/.cache/gravv${env}/stage-temp`),
};

export const hubInboxes = config.hubInboxes ? config.hubInboxes.map(d => resolve(d)) : [];

export const hubStorage = {
  files: resolve(config?.hubStorage?.files || `~/.data/gravv${env}/fileblobs`),
};

export const hubHost = config.hubHost ?? 'api';
export const hubPort = config.hubPort ?? 8080;
const hubPortUrlPart = `:${hubPort}`;
export const hubUrl = `https://${hubHost}${hubPortUrlPart}`;

// inbox watching settings
export const watchIdleMs = config.watchIdleMs || 1200000; // 20 mins
export const watchIntervalMs = config.watchIntervalMs || 1800000; // 30 mins
export const removeDirectoriesTimer = 30000; // toNearestMinute(config.removeDirectoriesTimer || 3600000); // 1 hr

export const eventsMax = config.eventsMax || 100;

export const receiptProperties = config.receiptProperties || [];

export const networkCheckInterval = config.networkCheckInterval || 60000;

export const fileWaitMs = config.fileWaitMs ?? 3000;

export const fileWaitPollMs = config.fileWaitPollMs ?? 500;

// Kafka settings

export const brokers = config.brokers || [`kafka:9092`];
export const clientId = config.clientId || 'gravv';
export const workerGroupId = config.workerGroupId || 'gravv-worker';
export const hubCleanupGroupId = config.hubCleanupGroupId || 'gravv-hub-cleanup';
export const initialRetryTime = config.initialRetryTime || 300;
export const retries = config.retries || 10;
// NOTHING, ERROR, WARN, INFO, and DEBUG
// Requires at least INFO-level application verbosity to see
export const kafkaLogLevel = Kafka.logLevel[config.kafkaLogLevel || 'ERROR'];

// Shared configuration

export const optUsage = argv.h;
export const optWatch = argv.w;

// Worker configuration

export const optProcessHub = argv.u;
// Use mocks to control verbosity in tests
export const optVerbosity = argv.v ?? config.verbosity;
export const optProcessHubFromBeginning = !falsyValues.includes(argv['process-from-beginning']);
export const optAllWorker = !optProcessHub && !optWatch;

// Hub configuration

export const optInstall = argv.i;
export const optCleanupReceived = argv.r;
export const optPopulate = argv.p;
export const optAllHub = !optInstall && !optWatch && !optCleanupReceived;
