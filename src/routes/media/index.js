import express from 'express';

import catchAll from '../../common/catch-all/index.js';
import * as media from '../../controllers/media/index.js';
import cast from '../../middleware/cast/index.js';
import validate from '../../middleware/validate/index.js';

export default express
  .Router()
  .get('/download', catchAll([
    validate(),
    media.download,
  ]))
  .post('/upload', catchAll([
    validate(),
    media.upload
  ]))
  .all('/search', (req, res, next) => {
    if (req.method === 'QUERY') {
      return catchAll([
	validate(),
	cast(),
	media.search,
      ])(req, res, next);
    }

    next();

    return undefined;
  })
  .get('/:hash', catchAll([
    validate(),
    media.get,
  ]))
  .patch('/:hash', catchAll([
    validate(),
    cast(),
    media.patch,
  ]))
  .delete('/:hash', catchAll([
    validate(),
    media.remove,
  ]));
