import {
  getRobots,
  getStatus,
} from '../../controllers/system/index.js';
import mediaRouter from '../media/index.js';

export default function routes(app) {
  app.get('/robots.txt', getRobots);
  app.get('/api/status', getStatus);
  app.use('/api/media', mediaRouter);
}
