import fs from 'fs';
import path from 'path';
import { Readable } from 'stream';
const SLASH = '_SL_';

// This is more of a data access object than a repository
class TempStorage {
  makePath(targetDir, sourceFile) {
    // Name the file to be both unique and readable
    let name = sourceFile.replace(/\//g, SLASH);
    // The name is required, so this is only defensive measures.
    if (!sourceFile) {
      name = Math.random() * 20;
    }
    return path.join(targetDir, name);
  }

  copyFrom(fileOrStream, targetDir, {
    name: sourceFile,
  }) {
    this.file = this.makePath(targetDir, sourceFile);

    let input = fileOrStream;
    if (!(fileOrStream instanceof Readable)) {
      input = fs.createReadStream(fileOrStream);
    }

    this.output = fs.createWriteStream(this.file);

    input.pipe(this.output);

    return new Promise((resolve, reject) => {
      this.output.on('finish', () => resolve(this.file));
      this.output.on('error', reject);
    });
  }
}

export default TempStorage;
