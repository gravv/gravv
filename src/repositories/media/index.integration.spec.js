/**
 * Tests the media and media metadata API as an HTTP client.
 * It ensures that various operations such as retrieving, adding, and deleting
 * media metadata work as expected,
 * and that the API endpoints handle different scenarios correctly.
 */

import fs from 'fs';
import path from 'path';
import { Readable, Writable } from 'stream';

import { Factory } from 'fishery';
import { afterAll, beforeEach, describe, expect, it, vi } from 'vitest';

import {
  getValueFromPath
} from '../../common/test/index.js';
import Media from '../../entities/media/index.js';
import MediaRepository from '../../repositories/media/index.js';
import logService from '../../services/log/index.js';
import dbService from '../../services/postgres/index.js';

const storagePath = '/tmp/files';
const db = dbService.getInstance();
const media = new MediaRepository(storagePath);

vi.spyOn(logService, 'info').mockReturnValue(vi.fn());
vi.spyOn(logService, 'error').mockReturnValue(vi.fn());
vi.spyOn(logService, 'debug').mockReturnValue(vi.fn());

beforeEach(async () => {
  vi.clearAllMocks();

  vi.spyOn(fs, 'createWriteStream').mockImplementation(() => {
    const stream = new Writable();
    stream._write = (chunk, encoding, done) => done();
    process.nextTick(() => stream.emit('finish'));
    return stream;
  });

  // Mock createReadStream
  vi.spyOn(fs, 'createReadStream').mockImplementation(() => {
    const stream = new Readable();
    stream._read = () => {};
    process.nextTick(() => stream.emit('data', 'abc'));
    process.nextTick(() => stream.emit('data', 'def'));
    process.nextTick(() => stream.emit('end'));
    return stream;
  });

  // Mock fs.promises.mkdir and fs.promises.rename
  vi.spyOn(fs.promises, 'mkdir').mockImplementation(() => Promise.resolve());
  vi.spyOn(fs.promises, 'rename').mockImplementation(() => Promise.resolve());

  await db('media').where({
    hash: '0'.padStart(64, '0')
  }).del();
});

afterAll(async () => {
  await db('media').where({
    hash: '0'.padStart(64, '0')
  }).del();
});

const mediaFactory = Factory.define(({ sequence, transientParams }) => {
  const { id, customProps } = transientParams;

  const now = new Date().toISOString();
  const media = {
    filename: `test-file-${id ?? sequence}.jpg`,
    hash: `${id ?? sequence}`.padStart(64, '0'),
    size: "1024",
    type: 'jpg',
    createdAt: now,
    modifiedAt: now,
    stat: {
      atime: now,
      birthtime: now,
      ctime: now,
      gid: 1000,
      mode: 111,
      mtime: now,
      uid: 1000,
    },
    ...customProps,
  };

  if (customProps?.exif) {
    media.exif = { ...media.exif, ...customProps.exif };
  }
  if (customProps?.ffmpeg) {
    media.ffmpeg = { ...media.ffmpeg, ...customProps.ffmpeg };
  }
  if (customProps?.identity) {
    media.identity = { ...media.identity, ...customProps.identity };
  }
  if (customProps?.imagemagick) {
    media.imagemagick = { ...media.imagemagick, ...customProps.imagemagick };
  }
  if (customProps?.stat) {
    media.stat = { ...media.stat, ...customProps.stat };
  }

  return Media.fromJSON(media);
});

const listExamples = [
  {
    name: 'Basic usage',
    inputs: {
      hash: '0'.padStart(64, '0'),
      data: mediaFactory.build({}, {
	transient: {
	  id: 0,
	}
      })
    },
  },
];

describe('List', () => {
  listExamples.forEach((example) => {
    it(example.name, async () => {
      await media.patch(example.inputs.data);

      const result = await media.search();

      expect(+result.pagination.total).gt(0);
    });
  });
});

const retrieveExamples = [
  {
    name: 'Basic usage',
    inputs: {
      hash: '0'.padStart(64, '0'),
      data: mediaFactory.build({}, {
	transient: {
	  id: 0,
	}
      })
    },
    want: {
      hasData: true,
    },
  },
  {
    name: 'Basic error',
    inputs: {
      hash: '10'.padStart(64, '0'),
      data: mediaFactory.build({}, {
	transient: {
	  id: 0, // not equal
	}
      })
    },
    want: { hasData: false }
  }
];

describe('Retrieve', () => {
  retrieveExamples.forEach((example) => {
    it(example.name, async () => {
      await media.patch(example.inputs.data);

      const result = await media.get(example.inputs.hash);
      if (example.want.hasData) {
	expect(result).toBeDefined();
	expect(result.hash).toEqual(example.inputs.hash);
      } else {
	expect(result).not.toBeDefined();
      }
    });
  });
});

const addExamples = [
  {
    name: 'Add new base properties',
    inputs: {
      data: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
	    size: "1000",
	  }
        }
      }),
      updateData: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
	    size: undefined,
            type: 'png',
	  }
        }
      })
    },
    want: {
      preserves: ['size'],
      additions: ['type'],
    },
  },
  {
    name: 'Update existing base properties',
    inputs: {
      data: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
	    size: "1000",
	  }
        }
      }),
      updateData: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
	    size: "2000",
            filename: "Another",
	  }
        }
      })
    },
    want: {
      updates: ['size'],
      additions: ['filename'],
    },
  },
  {
    name: 'Delete base properties',
    inputs: {
      data: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
	    size: "1000",
	  }
        }
      }),
      updateData: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
	    size: undefined,
            filename: null,
	  }
        }
      })
    },
    want: {
      defaultsAs: [['filename', ""]],
      preserves: ['size'],
    },
  },
  {
    name: 'Add new identity properties',
    inputs: {
      data: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
            identity: {
	      ext: 'Value 1',
	    }
	  }
        }
      }),
      updateData: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
            identity: {
              signature: 'New Value 1',
              sha256: 42,
            }
	  }
        }
      })
    },
    want: {
      preserves: ['identity.ext'],
      additions: ['identity.signature', 'identity.sha256'],
    },
  },
  {
    name: 'Add new identity properties - new grouping',
    inputs: {
      data: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
            identity: {
	      ext: 'Value 1',
	    }
	  }
        }
      }),
      updateData: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
            identity: {
              signature: 'New Value 1',
	    },
	    exif: {
	      FileSize: '1000',
            }
	  }
        }
      })
    },
    want: {
      preserves: ['identity.ext'],
      additions: ['identity.signature', 'exif.FileSize'],
    },
  },
  {
    name: 'Update existing identity properties',
    inputs: {
      data: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
            identity: {
	      ext: 'Old Value'
	    }
	  }
        }
      }),
      updateData: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
            identity: {
              ext: 'Updated Value',
              signature: 'Another Value'
            }
	  }
        }
      })
    },
    want: {
      updates: ['identity.ext'],
      additions: ['identity.signature']
    },
  },
  {
    name: 'Delete identity properties',
    inputs: {
      data: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
            identity: {
              ext: 'Delete me',
              signature: 'Keep me',
            }
	  }
        }
      }),
      updateData: mediaFactory.build({}, {
        transient: {
          id: 0,
	  customProps: {
            identity: {
              ext: null,
            }
          }
	}
      })
    },
    want: {
      deletions: ['identity.ext'],
      preserves: ['identity.signature']
    },
  },
];

describe('Add', () => {
  addExamples.forEach((example) => {
    it(example.name, async () => {
      await media.patch(example.inputs.data);

      // Update if updateData exists
      if (example.inputs.updateData) {
        await media.patch(example.inputs.updateData);
      }

      // Retrieve the media
      const result = await media.get(example.inputs.data.hash);

      expect(result).toBeDefined();

      // Check for additions
      if (example.want.additions) {
        example.want.additions.forEach(path => {
          const wantValue = getValueFromPath(example.inputs.updateData, path);
          const resultValue = getValueFromPath(result, path);
	  expect(resultValue, path).toBeDefined();
          expect(resultValue, path).toEqual(wantValue);
        });
      }

      // Check for updates
      if (example.want.updates) {
        example.want.updates.forEach(path => {
          const expectedValue = getValueFromPath(example.inputs.updateData, path);
          expect(result, path).toHaveProperty(path, expectedValue);
        });
      }

      // Check for deletions
      if (example.want.deletions) {
        example.want.deletions.forEach(path => {
	  expect(result, path).toBeDefined();
          expect(result, path).not.toHaveProperty(path);
        });
      }
      // Check for deletes that have defaults
      if (example.want.defaultsAs) {
        example.want.defaultsAs.forEach(([path, defaultValue]) => {
          const resultValue = getValueFromPath(result, path);
	  expect(resultValue, path).toBeDefined();
          expect(resultValue, path).toEqual(defaultValue);
        });
      }

      // Check for preserved properties
      if (example.want.preserves) {
        example.want.preserves.forEach(path => {
          const wantValue = getValueFromPath(example.inputs.data, path);
          const resultValue = getValueFromPath(result, path);
	  expect(resultValue, path).toBeDefined();
          expect(resultValue, path).toEqual(wantValue);
        });
      }
    });
  });
});

const removeExamples = [
  {
    name: 'Basic usage',
    inputs: {
      hash: '0'.padStart(64, '0'),
      id: 0,
    },
    want: { status: 200 },
  },
];

describe('Remove', () => {
  removeExamples.forEach((example) => {
    it(example.name, async () => {
      // First, add the media to ensure it exists
      const data = mediaFactory.build({}, {
	transient: {
	  id: example.inputs.id,
	}
      });

      await media.patch(data);

      // Now, remove the media
      await media.remove(data.hash, { hardRemove: true });

      // Finally, try to get the media and expect a 404
      const result = await media.get(data.hash);

      expect(result).not.toBeDefined();
    });
  });
});

describe('Upload', () => {
  it('should save a media file to the file storage', async () => {
    const mediaFile = fs.createReadStream('/dev/null');
    const result = await media.upload(mediaFile);
    expect(fs.createWriteStream).toHaveBeenCalledWith(
      expect.stringMatching(/^\/tmp\/\d{20}/)
    );

    // check against manually hashed 'abcdef'
    expect(result).to.equal('bef57ec7f53a6d40beb640a780a639c83bc29ac8a9816f1fc6c5c6dcd93c4721');
  });
});

describe('Download', () => {
  it('should retrieve a media file from the file storage', async () => {
    const hash = '0'.padStart(64, '0');
    const filePath = path.join(storagePath, hash.slice(0, 4), hash);

    media.download(hash);
    expect(fs.createReadStream).toHaveBeenCalledWith(filePath);
  });
});
