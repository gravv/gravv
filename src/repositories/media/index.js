import crypto from 'crypto';
import fs from 'fs';
import path from 'path';

import {
  hubStorage
} from '../../constants/core/index.js';
import Media from '../../entities/media/index.js';
import log from '../../services/log/index.js';
import dbService from '../../services/postgres/index.js';

const tableName = 'media';

class MediaRepository {
  /**
   * Initializes the MediaRepository with the given database configuration and file storage path.
   * @param {object} dbConfig - The configuration object for the database.
   * @param {string} fileStoragePath - The base path where media files will be stored.
   */
  constructor(storagePath) {
    this.db = dbService.getInstance();
    this.storagePath = storagePath;
  }

  /**
   * Retrieves a media file from the file storage.
   * @param {string} name - The name of the media file to be retrieved.
   *   By convention this is the file hash followed by labels for variants.
   * @param {fs.ReadStream} outputStream - The read stream of the api.
   * @returns {Promise<void>}
   */
  download(name) {
    const filePath = path.join(this.storagePath, name.slice(0, 4), name);
    const readStream = fs.createReadStream(filePath);

    return readStream;
  }

  /**
   * Saves a media file to the file storage.
   * @param {object} mediaFile - The media file object containing the stream and hash.
   * @returns {Promise<void>}
   */
  async upload(inputStream, { hashOverride, name='' }={}) {
    const randomId = `${Math.round(Math.random() * 1e20)}`.padStart(20, '0');
    const tempFilePath = path.join('/tmp', randomId);
    const writeStream = fs.createWriteStream(tempFilePath);
    const hash = crypto.createHash('sha256');

    return new Promise((resolve, reject) => {
      inputStream.pipe(writeStream);

      inputStream.on('data', (chunk) => {
        hash.update(chunk);
      });

      inputStream.on('end', async () => {
        const finalHash = hashOverride ?? hash.digest('hex');

        const finalFilePath = path.join(hubStorage.files, finalHash.slice(0, 4), finalHash + name);

	const dir = path.dirname(finalFilePath);
	await fs.promises.mkdir(dir, { recursive: true });
        await fs.promises.rename(tempFilePath, finalFilePath);

        resolve(finalHash);
      });

      inputStream.on('error', (error) => {
        reject(error);
      });
    });
  }

  /**
   * Retrieves media metadata from the database.
   * @param {string} hash - The hash of the media file whose details are to be retrieved.
   * @returns {Promise<object>} - The media details object.
   */
  async get(hash) {
    const hasResult = this.db(tableName)
      .where({ hash })
      .andWhere('deletedAt', null)
      .first();

    const result = await hasResult;

    if (!result) return undefined;

    const media = Media.fromJSON(result);

    return media;
  }

  /**
   * Removes media metadata from the database.
   * @param {string} hash - The hash of the media file to be removed.
   * @param {boolean} hardRemove - Whether to perform a hard remove.
   * @returns {Promise<void>}
   */
  async remove(hash, { hardRemove } = {}) {
    if (hardRemove) {
      await this.db(tableName).where({ hash }).del();
    } else {
      await this.db(tableName).where({ hash }).update({ deletedAt: new Date().toISOString() });
    }
  }

  /**
   * Saves media metadata to the database.
   *
   * This method performs a quasi-idempotent merge of the provided media data into the `media` table.
   * It supports partial updates, handling JSONB columns, and applying sensible defaults for non-JSON attributes.
   * Supply null to delete a property.
   * Missing or undefined properties are ignored.
   *
   * @param {object} mediaDetails - The media details object to be saved.
   * @returns {Promise<void>}
   *
   * @example
   * // Example input where some fields are null or missing
   * const mediaDetails = {
   *   hash: '123abc',
   *   filename: null,
   *   size: null,
   *   type: 'image/png',
   *   exif: {
   *     cameraModel: null,
   *     resolution: '1024x768'
   *   },
   *   identity: null,
   *   createdAt: null
   * };
   *
   * // Calling the set method
   * await mediaRepository.set(mediaDetails);
   *
   * // The resulting database entry will be:
   * // {
   * //   hash: '123abc',
   * //   filename: '',                 // Set to default value
   * //   size: 0,                      // Set to default value
   * //   type: 'image/png',            // Kept as provided
   * //   exif: {
   * //     cameraModel: null,          // Property set to null
   * //     resolution: '1024x768'      // Kept as provided
   * //   },
   * //   ffmpeg: {},                   // Set to empty JSON object
   * //   identity: {},                 // Set to empty JSON object
   * //   imagemagick: {},              // Set to empty JSON object
   * //   stat: {},                     // Set to empty JSON object
   * //   createdAt: '1970-01-01T00:00:00Z', // Set to default value
   * //   modifiedAt: current date and time, // Set to default value
   * //   deletedAt: null               // Remains default value
   * // }
   */
  async patch(media) {
    const jsonbColumns = ['exif', 'ffmpeg', 'identity', 'imagemagick', 'stat'];

    const mediaIncoming = media.toJSON({ collections: 'keyValue' });
    if (!mediaIncoming.hash) {
      throw new Error('Hash is required for patching mediaIncoming data.');
    }

    await this.db.transaction(async (trx) => {

      // Handle non-JSONB columns
      const baseIncoming = Object.fromEntries(
	Object.entries(mediaIncoming)
	  .filter(([prop]) => {
	    return !jsonbColumns.includes(prop);
	  })
	  .map(([prop, value]) => {
            return [prop, value === null ? trx.raw('DEFAULT') : value];
	  })
      );

      // Fetch current JSONB values and merge with cleaned data
      const mediaCurrent = (await trx(tableName)
	.select()
	.where('hash', mediaIncoming.hash)
	.first()) ?? {};

      const mediaNew = jsonbColumns.reduce((acc, column) => {
	// ie. "identity" or "stat"
        acc[column] = {
          ...mediaCurrent[column],
        };

	for (const prop in mediaIncoming[column]) {
	  // undefined will clobber when using spread
	  const isNull = mediaIncoming[column][prop] === null;
          if (mediaIncoming[column][prop]) {
	    acc[column][prop] = mediaIncoming[column][prop];
	  } else if (isNull) {
	    delete acc[column][prop];
	  }
	}

	return acc;
      }, baseIncoming);

      log.debug('Patch before final query', { mediaCurrent, mediaIncoming, mediaNew });

      const [updatedMedia] = await trx(tableName)
        .insert({
          ...mediaNew,
          deletedAt: null,
        })
        .onConflict('hash')
        .merge(mediaNew)
        .returning('*');
      return updatedMedia;
    });
  }

  async search(criteria={}) {
    let query = this.db(tableName);

    // Get the total count of results
    const countQuery = query.clone().count({ total: '*' });
    const totalResults = await countQuery;
    const total = totalResults[0].total;

    // Filtering
    if (criteria.filters) {
      for (const [field, value] of Object.entries(criteria.filters)) {
        query = query.where(field, value);
      }
    }

    // Sorting
    if (criteria.sortBy) {
      for (const [field, direction] of Object.entries(criteria.sortBy)) {
        query = query.orderBy(field, direction);
      }
    }

    // Pagination
    const page = criteria.page || 1;
    const pageSize = criteria.pageSize || 10;
    query = query.limit(pageSize).offset((page - 1) * pageSize);

    query = query.andWhere('deletedAt', null);

    // Execute
    const resultsRaw = await query;

    const results = resultsRaw.map(Media.fromJSON);

    return {
      data: results,
      pagination: {
        page,
        pageSize,
        total,
      },
    };
  }
}

export default MediaRepository;
