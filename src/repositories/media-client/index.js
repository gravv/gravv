import fs from 'fs';
import https from 'https';
import { Readable } from 'stream';

import axios from 'axios';

class MediaClient {
  constructor(baseUrl) {
    this.api = axios.create({
      baseURL: `${baseUrl}/api/media`,
      httpsAgent: new https.Agent({
	rejectUnauthorized: false,
      }),
    });
  }

  upload(input, metadata={}) {
    let inputStream;
    if (input instanceof Readable) {
      inputStream = input;
    } else {
      inputStream = fs.createReadStream(input);
    }

    const extraHeaders = {};

    if (metadata.name) {
      extraHeaders['Media-Hash'] = metadata.hash;
      extraHeaders['Media-Name'] = metadata.name;
    }

    return this.api.post('/upload', inputStream, {
      headers: {
        'Content-Type': 'application/octet-stream',
	...extraHeaders,
      },
      maxContentLength: Infinity,
      maxBodyLength: Infinity,
    });
  }

  download(hash) {
    return this.api.get(`/download/${hash}`, {
      responseType: 'stream',
    });
  }

  async get(hash) {
    const response = await this.api.get(`/${hash}`);
    return response.data;
  }

  async remove(hash, hardRemove = false) {
    await this.api.delete(`/${hash}`, {
      data: { hardRemove },
    });
  }

  async set(metadata) {
    await this.api.patch(`/${metadata.hash}`, metadata);
  }

  async search(criteria) {
    const response = await this.api.post('/search', criteria);
    return response.data;
  }
}

export default MediaClient;
