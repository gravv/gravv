import path from 'path';

import { bufferType, stat } from '../../common/files/index.js';
import { validateFile } from '../../common/media/index.js';
import Media from '../../entities/media/index.js';
import Identity from '../../values/identity-data/index.js';
import Stat from '../../values/stat-data/index.js';
import log from '../log/index.js';

class QuickScanService {
  async scan(file, { hash, machineId, sourceFile }={}) {
    const isValid = await validateFile(file);
    if (!isValid) {
      log.debug('[quick scan: invalid file]');
      return undefined;
    }

    log.debug('[stat file]');
    const statResult = await stat(file);

    log.debug('[load extension]');
    const ext = (await bufferType(file)) ?? path.extname(file).slice(1);

    const identity = {
      ext,
      file: sourceFile ?? file,
      hash,
      machineId,
      machineTZ: process.env.TZ,
    };

    const result = new Media({
      filename: path.basename(sourceFile ?? file),
      hash,
      size: statResult.size,
      type: identity.ext,
      createdAt: statResult.birthtime,
      modifiedAt: statResult.mtime,
      stat: new Stat(statResult),
      identity: new Identity(identity),
    });

    return result;
  };
}

export default QuickScanService;
