import dotenv from 'dotenv';
import knex from 'knex';
import { knexSnakeCaseMappers } from 'objection';

import log from '../log/index.js';

dotenv.config({ path: `./.env-${process.env.NODE_ENV}` });

class DBService {
  constructor() {
    this.instance = null; // Initialize instance as null
  }

  getInstance(config) {
    if (!this.instance) {
      const user = process.env.POSTGRES_USER;
      const password = process.env.POSTGRES_PASSWORD;
      const port = process.env.POSTGRES_PORT;
      const database = 'media';

      const connection = `postgres://${user}:${password}@postgres:${port}/${database}`;

      this.instance = knex({
        client: 'pg',
        connection,
	rejectUnauthorized: true,
	...knexSnakeCaseMappers(),
	...config,
      });
    }

    this.instance.on('query-error', (error, query) => {
      log.error(error, 'error');
      log.error(query, 'query');
    });

    return this.instance;
  }

  // Can invoke on app startup
  connect(config) {
    return this.getInstance(config);
  }
}

const dbService = new DBService();

export default dbService;
