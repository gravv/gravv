import fsCb from 'fs';
import path from 'path';

import express from 'express';

import {
  collectDir,
  exists,
} from '../../common/files/index.js';
import {
  setupTopics,
} from '../../common/kafka/index.js';
import {
  status,
} from '../../common/lib/index.js';
import {
  hubStorage,
} from '../../constants/core/index.js';
import {
  mediaEventConfig,
} from '../../constants/kafka-topics/index.js';
import methodOverride from '../../middleware/method-override/index.js';
import routesTopLevel from '../../routes/api/index.js';
import log from '../../services/log/index.js';

const fs = fsCb.promises;

class ExpressApi {
  constructor({
    routes = routesTopLevel,
    kafkaClient,
  }) {
    this.routes = routes;
    this.kafkaClient = kafkaClient;
  }

  async setup () {
    log.debug(`Hub setup ${status.debug}: Create topics, create inboxes, storage area, metadata database`);

    // Create Kafka topics manually in order to specify configuration
    // Automatic creation won't have the right values
    log.info(`Kakfa topics create ${status.started}`);

    const isTopicsCreated = await setupTopics([
      mediaEventConfig,
    ], { client: this.kafkaClient });


    if(isTopicsCreated) {
      log.info(`Kakfa topics create ${status.complete}`);
    }

    log.info(`Storage setup ${status.started}. This may take a few minutes. (files: ${hubStorage.files})`);

    const hubStorageDirExists = await exists(hubStorage.files);
    if (hubStorageDirExists) {
      const files = await collectDir(`${hubStorage.files}/**/*`);
      const foundMessage = `Found ${files.length} files`;
      log.info(`Storage setup: ${foundMessage} (dir: ${hubStorage.files})`);
    } else {
      await fs.mkdir(hubStorage.files, { recursive: true });
    }

    // To support more than 100K files, each file is stored in a directory of their prefix
    // Rsync won't create the directories if they don't already exist
    const hubStorageDirFolders = await fs.readdir(hubStorage.files);
    const foldersLength = hubStorageDirFolders
	  .filter(f => /^[a-f0-9]{4}$/.test(f))
	  .length;

    if (foldersLength < 65535) {
      const maxFourDigitHex = 65535;
      for (let i = 0; i <= maxFourDigitHex; i++) {
	const prefix = Number(i).toString(16).padStart(4, '0');

	await fs.mkdir(path.resolve(hubStorage.files, prefix), { recursive: true });
      }
    }

    log.info(`Storage setup ${status.complete} (dir: ${hubStorage.files})`);

    log.debug(`Hub setup ${status.complete}`);

    return {
      isTopicsCreated,
    };
  }

  async populate () {
    // TODO unimplemented
  }

  async build({ setup, populate }={}) {
    let needsPopulate;
    if (setup) {
      const {
	isTopicsCreated
      } = await this.setup();

      needsPopulate = isTopicsCreated;
    }
    if (populate || needsPopulate) {
      this.populate();
    }

    const app = express();

    app.use(methodOverride);
    app.use(express.json());

    app.use((req, res, next) => {
      log.info(`${req.method} ${req.url}`);
      next();
    });
    this.routes(app);

    // eslint-disable-next-line no-unused-vars
    app.use((error, req, res, next) => {
      log.error(`Unhandled Express Error: ${error.message} - ${req.method} ${req.url}`);

      if (['test', 'development'].includes(process.env.NODE_ENV)) {
        log.error(error.stack.split('\n'));
      }

      res.status(error.status || 500).json({
        error: {
          message: error.message,
          ...(process.env.NODE_ENV !== 'production' && { stack: error.stack.split('\n') })
        }
      });
    });

    return app;
  }
}


export default ExpressApi;
