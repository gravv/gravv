/* eslint-disable no-console */

import {
  isTest,
  optVerbosity,
} from '../../constants/core/index.js';

// Define log levels
export const LOG_LEVELS = {
  NONE: 0,
  ERROR: 1,
  WARN: 2,
  INFO: 3,
  DEBUG: 4
};

export const LOG_LEVEL_NAMES = {
  0: 'NONE',
  1: 'ERROR',
  2: 'WARN',
  3: 'INFO',
  4: 'DEBUG'
};

let currentVerbosity;

if (isTest) {
  currentVerbosity = LOG_LEVELS.DEBUG;
}
if (typeof currentVerbosity === 'undefined') {
  currentVerbosity = optVerbosity ?? LOG_LEVELS.WARN;
}

// Create logger function
const createLogger = (level, levelName) => (message, ...args) => {
  if (level > currentVerbosity) {
    return;
  }
  console.error(`[${levelName}] ${message}`, ...args);
};

// Create logger object
const logger = {
  debug: createLogger(LOG_LEVELS.DEBUG, 'DEBUG'),
  info: createLogger(LOG_LEVELS.INFO, 'INFO'),
  warn: createLogger(LOG_LEVELS.WARN, 'WARN'),
  error: createLogger(LOG_LEVELS.ERROR, 'ERROR'),

  verbosity: (v) => {
    if (LOG_LEVELS.NONE <= v && v <= LOG_LEVELS.DEBUG) {
      currentVerbosity = v;
    } else {
      console.error(`Invalid verbosity level: ${v}. Using default (2).`);
    }
  },
  getVerbosity: () => currentVerbosity
};

export default logger;
