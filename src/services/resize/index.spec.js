import fs from 'fs/promises';
import path from 'path';

import gm from 'gm';
import { afterAll, afterEach, beforeEach, describe, expect, it, vi } from 'vitest';

import * as ffmpegModule from '../../common/ffmpeg-enhanced/index.js';
import * as constants from '../../constants/core/index.js';
import log from '../../services/log/index.js';
import ResizeService from '../../services/resize/index.js';

// Constants for test file paths
const TEST_ROOT = path.join(process.cwd(), '.test', 'resize');
const GOOD_FILES_PATH = path.join(process.cwd(), 'samples/test-good');
const EXT_FILES_PATH = path.join(process.cwd(), 'samples/test-ext');

vi.spyOn(log, 'info').mockImplementation(() => {});
vi.spyOn(log, 'error').mockImplementation(() => {});
vi.spyOn(log, 'debug').mockImplementation(() => {});

vi.mock('../../constants/core/index.js', async (importOriginal) => ({
  ...(await importOriginal()),
}));

beforeEach(async () => {
  vi.clearAllMocks();
  await fs.mkdir(TEST_ROOT, { recursive: true });
  constants.storage = {
    ...constants.storage,
    stageTemp: TEST_ROOT,
  };
});

afterEach(async () => {
  await fs.rm(TEST_ROOT, { recursive: true, force: true });
});

afterAll(async () => {
  await fs.rm(TEST_ROOT, { recursive: true, force: true });
});

const resizeExamples = [
  {
    name: 'Resize image',
    inputs: {
      media: {
        hash: 'testhash',
        type: 'jpg',
	imagemagick: {

	},
        exif: {
          SourceFile: path.join(GOOD_FILES_PATH, 'good.jpg'),
        },
      },
      resizes: {
        'image/jpeg': {
          thumbnail: { size: 100 },
          medium: { size: 500 },
        },
      },
    },
    want: {
      hasVideos: false,
      hasImages: true,
      imageCount: 2,
    },
  },
  {
    name: 'Resize video',
    inputs: {
      media: {
        hash: 'testhash',
        type: 'mp4',
	ffmpeg: {
	  width: 1000,
	},
        exif: {
          SourceFile: path.join(GOOD_FILES_PATH, 'good.mp4'),
        },
      },
      resizes: {
        'video/mp4': {
          low: { size: 360 },
          high: { size: 720 },
        },
        'image/jpeg': {
          thumbnail: { size: 100 },
        },
      },
    },
    want: {
      hasVideos: true,
      hasImages: true,
      videoCount: 2,
      imageCount: 1,
    },
  },
  {
    name: 'Unsupported file type',
    inputs: {
      media: {
        hash: 'testhash',
        type: 'txt',
        exif: {
          SourceFile: path.join(EXT_FILES_PATH, 'txt.md'),
        },
      },
      resizes: {},
    },
    want: {
      hasVideos: false,
      hasImages: false,
    },
  },
];

describe('ResizeService', () => {
  resizeExamples.forEach((example) => {
    it(example.name, async () => {
      const resizeService = new ResizeService();
      const result = await resizeService.scan(example.inputs.media, example.inputs.resizes);

      if (example.want.hasVideos) {
        expect(result.videos).toHaveLength(example.want.videoCount);
        result.videos.forEach((video) => {
          expect(video.path).toBeDefined();
          expect(fs.access(video.path)).resolves.not.toThrow();
        });
      } else {
        expect(result.videos).toHaveLength(0);
      }

      if (example.want.hasImages) {
        expect(result.images).toHaveLength(example.want.imageCount);
        result.images.forEach((image) => {
          expect(image.path).toBeDefined();
          expect(fs.access(image.path)).resolves.not.toThrow();
        });
      } else {
        expect(result.images).toHaveLength(0);
      }

      if (example.want.hasVideos || example.want.hasImages) {
        expect(result.crop).toBeDefined();
        expect(result.crop.name).toBe('smartcrop');
      }
    });
  });

  it('should handle errors during video resizing', async () => {
    vi.spyOn(ffmpegModule, 'default').mockImplementation(() => {
      throw new Error('FFMPEG error');
    });

    const resizeService = new ResizeService();
    const media = {
      hash: 'testhash',
      type: 'mp4',
      ffmpeg: {
	width: 1000,
      },
      exif: {
        SourceFile: path.join(GOOD_FILES_PATH, 'video.mp4'),
      },
    };
    const resizes = {
      'video/mp4': {
        low: { width: 480, height: 360 },
      },
    };

    await expect(resizeService.scan(media, resizes)).rejects.toThrow('FFMPEG error');
  });

  it('should handle errors during image conversion', async () => {
    vi.spyOn(gm.prototype, 'write').mockImplementation(function (path, callback) {
      callback(new Error('Conversion error'));
    });

    const resizeService = new ResizeService();
    const media = {
      hash: 'testhash',
      type: 'bmp',
      exif: {
        SourceFile: path.join(GOOD_FILES_PATH, 'good.bmp'),
      },
    };
    const resizes = {
      'image/jpg': {
        thumbnail: { width: 100, height: 100 },
      },
    };

    await expect(resizeService.scan(media, resizes)).rejects.toThrow('Conversion error');
  });
});
