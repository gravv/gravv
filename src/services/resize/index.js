import path from 'path';

import * as R from 'ramda';
import smartcrop from 'smartcrop-sharp';

import {
  resize as resizeVideo,
} from '../../common/ffmpeg/index.js';
import ffmpeg from '../../common/ffmpeg-enhanced/index.js';
import {
  removeFile,
  removeOnError,
  extTarget,
} from '../../common/files/index.js';
import {
  convert as convertImage,
} from '../../common/graphicsmagick/index.js';
import {
  toSlug,
} from '../../common/lib/index.js';
import {
  resize as resizeImage,
  scaleFactor,
  toMimeExt,
} from '../../common/sharp/index.js';
import {
  storage,
} from '../../constants/core/index.js';
import {
  bufferTypesImage,
  bufferTypesVideo,
  bufferTypesSupported,
  targetVideoType,
  targetImageType,
} from '../../constants/files/index.js';
import log from '../log/index.js';

const prefixWith = (prefix) => def => ({ ...def, name: `${prefix}${def.name}` });

const zipWithPaths = (paths) => (def, index) => ({ ...def, path: paths[index] });

class ResizeService {
  async scan(media, resizes = {}) {
    const isSupported = bufferTypesSupported.includes(media.type);
    const isVideo = isSupported && bufferTypesVideo.includes(media.type);
    const isImage = isSupported && bufferTypesImage.includes(media.type);

    if (!isImage && !isVideo) {
      return { videos: [], crop: undefined, images: [] };
    }

    let derivedImage;

    let videos = [];
    const videoResizes = Object.values(resizes[`video/${toMimeExt(targetVideoType)}`] ?? {});
    if (isVideo) {
      log.info('[resize: resize video]');

      videos = await Promise.all(videoResizes.map((resizeDef) => {
	const filename = `${media.hash}---${toSlug(resizeDef)}.${targetVideoType}`;
	const outputPath = path.join(storage.stageTemp, filename);

	return resizeVideo(media, resizeDef, outputPath);
      }));

      log.info('[resize: extract main image (video cover)]');

      const coverOutputPath = path.join(storage.stageTemp, `${media.hash}-cover.${targetImageType}`);

      derivedImage = await removeOnError(async () => {
	await ffmpeg(media.exif.SourceFile)
	  .extractCover({ offset: 1000 })
	  .output(coverOutputPath)
	  .run();

	return coverOutputPath;
      }, [coverOutputPath]);
    }

    const extNew = extTarget(media.type);
    if (isImage && media.type !== extNew) {
      log.info('[resize: convert image type]');
      const outputPath = path.join(storage.stageTemp, `${media.hash}.${extNew}`);
      derivedImage = await convertImage(media, outputPath);
    }

    const mainImage = derivedImage ?? media.exif.SourceFile;
    let cropFound;
    try {
      const {
	topCrop,
      } = await smartcrop.crop(mainImage, { width: 100, height: 100 });
      cropFound = topCrop;
    } catch (error) {
      // SmartCrop does not return a real error
      throw new Error(error.message);
    }
    const crop = {
      name: 'smartcrop',
      ...cropFound,
      x: cropFound.x * scaleFactor(media),
      y: cropFound.y * scaleFactor(media),
      width: cropFound.width * scaleFactor(media),
      height: cropFound.height * scaleFactor(media),
    };

    log.info('[resize: resize main image]');

    const extImage = toMimeExt(isVideo ? 'jpg' : extNew);
    const imageResizes = Object.values(resizes[`image/${extImage}`] ?? {});
    const images = await Promise.all(
      R.xprod([mainImage], imageResizes).map(([image, resizeDef]) => {
	const resizePath = path.join(storage.stageTemp, `${media.hash}---${toSlug(resizeDef)}.${extNew}`);
	return resizeImage(media, image, resizeDef, extImage, resizePath);
      })
    );

    for (const file of [derivedImage]) {
      await removeFile(file);
    }

    log.info('[resize: complete]');

    const zippedImages = imageResizes
	  .map(prefixWith(isVideo ? 'cover-' : '', imageResizes))
	  .map(zipWithPaths(images));
    const zippedVideos = videoResizes
	  .filter(() => videos.length)
	  .map(zipWithPaths(videos));

    return {
      videos: zippedVideos,
      crop,
      images: zippedImages,
    };
  }
}

export default ResizeService;
