import fs from 'fs/promises';

import through from 'through2';

import {
  collectDir,
  exists,
  removeEmptyDirectories,
  setIntervalAlt,
  watchDirRestartable,
} from '../../common/files/index.js';
import {
  status,
} from '../../common/lib/index.js';
import {
  removeDirectoriesTimer,
  watchIdleMs,
  watchIntervalMs,
} from '../../constants/core/index.js';
import log from '../../services/log/index.js';

export default class InboxWorker {
  constructor(inboxes) {
    this.inboxes = inboxes ?? [];
    this.inboxesFound = [];
    this.fileHandler = () => {};
    this.closeFns = [() => null];
    this.intervalIds = [];
  }

  async start() {
    this.inboxesFound = await this.setup();

    this.intervalRemove = await setIntervalAlt(async () => {
      log.info(`Removing empty inbox directories. Runs every ${Math.round(removeDirectoriesTimer / (1000 * 60))} minutes`);

      const allRemovals = this.inboxesFound.map(async (inboxDir) => {
        try {
          await removeEmptyDirectories(inboxDir);
        } catch (error) {
          log.info(`Remove empty inbox directories ${status.error}: ${error.message} (inboxDir: ${inboxDir})`);
          throw error;
        }
      });

      await Promise.all(allRemovals);

      log.debug(`Remove empty inbox directories ${status.complete}`);
    }, removeDirectoriesTimer, { leading: true });

    await this.watchAll();

    return this.inboxesFound;
  }


  async setup() {
    if (!this.inboxes.length) {
      log.warn(`Inbox setup ${status.skipped}: no inboxes found`);
    }

    const inboxesFound = await Promise.all(
      this.inboxes.map(async (inboxDir) => {
	const inboxDirExists = await exists(inboxDir);
	if (inboxDirExists) {
	  const files = await collectDir(`${inboxDir}/**/*`);
	  log.info(`Inbox setup ${status.debug}: Found ${files.length} files (inboxDir: ${inboxDir})`);
	  return inboxDir;
	} else {
	  try {
            await fs.mkdir(inboxDir, { recursive: true });
            log.info(`Inbox setup ${status.created}: Directory created (inboxDir: ${inboxDir})`);
            return inboxDir;
	  } catch (error) {
            log.info(`Inbox setup ${status.error}: ${error.message} (inboxDir: ${inboxDir})`);
	  }
	}
	const inboxExistsAfterCreating = await exists(inboxDir);
	return inboxExistsAfterCreating ? inboxDir : null;
      })
    );

    const inboxesFoundFiltered = inboxesFound.filter(Boolean);

    return inboxesFoundFiltered;
  }

  async watchAll() {
    return new Promise((resolve) => {
      this.inboxesFound.map((inboxDir) => {
	const {
	  fileStream,
	  intervalId,
	  close: close,
	} = watchDirRestartable(inboxDir, {
          cbStart: () => log.info(`Inbox watch ${status.started} (inboxDir: ${inboxDir})`),
          cbComplete: () => {
	    log.info(`Inbox watch ${status.complete} (inboxDir: ${inboxDir})`);
	    // TODO consider returning sooner
	    resolve();
	  },
          idleMs: watchIdleMs,
          intervalMs: watchIntervalMs,
	});

	this.closeFns.push(close);
	this.intervalIds.push(intervalId);

	fileStream.pipe(through.obj(async (file, enc, cb) => {
	  await this.fileHandler(file);
	  cb();
	}));
      });
    });
  }

  onFile(fileHandler) {
    this.fileHandler = fileHandler;
  }

  async cleanup() {
    this.intervalIds.forEach(interval => clearInterval(interval));
    await Promise.all(this.closeFns.map(fn => fn()));

    log.info(`Inbox worker cleanup ${status.complete}`);
  }
}
