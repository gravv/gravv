/* Tests the file watching infrastructure */

import fs from 'fs/promises';
import path from 'path';

import { afterAll, afterEach, beforeEach, describe, it, expect, vi } from 'vitest';

import retryWith from '../../common/retry/index.js';
import * as constants from '../../constants/core/index.js';
import producerService from '../kafka-producer/index.js';
import logService from '../log/index.js';

import InboxWorker from './index.js';

// Make the constants mutable
vi.mock('../../constants/core/index.js', async (importOriginal) => ({
  ...(await importOriginal()),
}));

const TEST_ROOT = path.join(process.cwd(), '.test', 'inbox');
const TEST_INBOX1 = path.join(TEST_ROOT, 'inbox1');
const TEST_INBOX2 = path.join(TEST_ROOT, 'inbox2');

const createTestFile = async (filePath, content = '') => {
  await fs.mkdir(path.dirname(filePath), { recursive: true });
  await fs.writeFile(filePath, content);
};

beforeEach(async () => {
  vi.spyOn(logService, 'info').mockReturnValue(vi.fn());
  vi.spyOn(logService, 'error').mockReturnValue(vi.fn());
  vi.spyOn(logService, 'debug').mockReturnValue(vi.fn());

  Object.assign(constants, {
    inboxes: [
      path.resolve(`.test/inbox1`),
      path.resolve(`.test/inbox2`)
    ],
    fileWaitMs: 10,
    fileWaitPollMs: 10,
    optVerbosity: 3,
  });

  vi.spyOn(producerService, 'getInstance')
    .mockImplementation(async () => {
      return {
	connect: vi.fn(() => Promise.resolve()),
	send: vi.fn(() => Promise.resolve()),
	on: vi.fn(),
      };
    });

  await fs.rm(TEST_ROOT, { recursive: true, force: true });
});
afterEach(async () => {
  vi.clearAllMocks();
  await fs.rm(TEST_ROOT, { recursive: true, force: true });
});
afterAll(() => {
  vi.restoreAllMocks();
});

describe('InboxWorker', () => {
  const testCases = [
    {
      description: 'should process files in a single inbox',
      inputs: {
        inboxPaths: [TEST_INBOX1],
        files: [
          {
	    path: path.join(TEST_INBOX1, 'test-file.txt'),
	    content: 'test content'
	  },
        ],
      },
      want: {
        calls: [path.join(TEST_INBOX1, 'test-file.txt')],
      }
    },
    {
      description: 'should process files in multiple inboxes',
      inputs: {
        inboxPaths: [TEST_INBOX1, TEST_INBOX2],
        files: [
          {
	    path: path.join(TEST_INBOX1, 'test-file1.txt'),
	    content: 'test content 1'
	  },
          {
	    path: path.join(TEST_INBOX2, 'test-file2.txt'),
	    content: 'test content 2'
	  },
        ],
      },
      want: {
        calls: [
          path.join(TEST_INBOX1, 'test-file1.txt'),
          path.join(TEST_INBOX2, 'test-file2.txt'),
        ],
      }
    },
  ];

  it('should setup inboxes', async () => {
    const inboxes = [TEST_INBOX1, TEST_INBOX2];
    const worker = new InboxWorker(inboxes);
    try {
      const inboxesFound = await worker.start();
      expect(inboxesFound).toEqual(inboxes);
    } finally {
      worker.cleanup();
    }
  });

  testCases.forEach(({ description, inputs, want }) => {
    it(description, async () => {
      const worker = new InboxWorker(inputs.inboxPaths);
      try {
	const processFile = vi.fn();

	worker.onFile(retryWith(processFile));

	await worker.start();

	for (const file of inputs.files) {
          await createTestFile(file.path, file.content);
	}

	// Wait for file processing
	await new Promise(resolve => setTimeout(resolve, 50));

	want.calls.forEach(expectedCall => {
          expect(processFile).toHaveBeenCalledWith(expectedCall);
	});
      } finally {
	await worker.cleanup();
      }
    }, 10000);
  });
});
