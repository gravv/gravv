import {
  makeClient,
  makeProducer,
} from '../../common/kafka/index.js';

const defaultConfig = {
  retry: {
    retries: 10, // For kafka re-elections, etc
  },
};

class KafkaProducerService {
  constructor() {
    this.instance = null; // Initialize instance as null
  }

  getInstanceSync(config=defaultConfig, { shouldConnect=true }={}) {
    if (!this.instance) {
      const client = makeClient(config);

      const producer = makeProducer(client);

      if (shouldConnect) {
	// connects async
	producer.connect();
      }

      this.instance = producer;
    }

    return this.instance;
  }

  async getInstance(config) {
    if (!this.instance) {
      this.instance = this.getInstanceSync(config, { shouldConnect: false });
      await this.instance.connect();
    }

    return this.instance;
  }

  // Can invoke on app startup
  connect(config) {
    return this.getInstanceSync(config);
  }
}

export const Service = KafkaProducerService;

const kafkaProducerService = new KafkaProducerService();

export default kafkaProducerService;
