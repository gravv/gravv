import path from 'path';

import * as R from 'ramda';

import { loadAndRepairExif } from '../../common/exif/index.js';
import {
  collect as collectFfmpeg,
  version as ffmpegVersion,
} from '../../common/ffmpeg/index.js';
import {
  bufferType,
  removeFile,
  stat,
} from '../../common/files/index.js';
import {
  collect,
  version as graphicsmagickVersion,
} from '../../common/graphicsmagick/index.js';
import {
  earliestDate,
  status,
} from '../../common/lib/index.js';
import { validateFile } from '../../common/media/index.js';
import { sha256Sum } from '../../common/sha-256-sum/index.js';
import Media from '../../entities/media/index.js';
import Exif from '../../values/exif-data/index.js';
import Ffmpeg from '../../values/ffmpeg-data/index.js';
import Identity from '../../values/identity-data/index.js';
import Imagemagick from '../../values/imagemagick-data/index.js';
import Stat from '../../values/stat-data/index.js';
import log from '../log/index.js';

class DeepScanService {
  async scan(file, {
    machineId,
  }={}) {
    const isValid = await validateFile(file);
    if (!isValid) {
      log.debug('[deep scan: invalid file]');
      return undefined;
    }

    try {
      this.artifacts = [`${file}_exiftool_tmp`];

      log.debug('[deep scan: stat]');
      const statResult = await stat(file);

      log.debug('[deep scan: repair exif]');
      const exifResult = await loadAndRepairExif(file, { statResult });

      log.debug('[deep scan: load extension]');
      const ext = (await bufferType(file)) || path.extname(file).slice(1) || undefined;

      log.debug('[deep scan: sha256]');
      const sha256 = await sha256Sum(file);

      log.debug('[deep scan: imagemagick]');
      const imagemagickResult = await collect(file);

      log.debug('[deep scan: ffmpeg]');
      const ffmpegResult = await collectFfmpeg(file);

      log.debug('[deep scan: imagemagick signature]');
      const signature = R.path(['signature'], imagemagickResult);

      const identity = {
	ext,
	machineId,
	sha256,
	signature,
	machineTZ: process.env.TZ,
      };

      return new Media({
	filename: path.basename(file),
	hash: sha256,
	size: statResult.size,
	type: identity.ext,
	createdAt: earliestDate([
	  stat.birthtime,
	  exifResult.CreateDate,
	  exifResult.DateTimeOriginal,
	  exifResult.FileModifyDate,
	  exifResult.ModifyDate
	]),
	modifiedAt: statResult.mtime,
	exif: new Exif(exifResult),
	imagemagick: new Imagemagick(imagemagickResult),
	ffmpeg: new Ffmpeg(ffmpegResult),
	identity: new Identity(identity),
	stat: new Stat(statResult),
      });
    } catch (error) {
      throw error;
    } finally {
      const filesToDelete = [this.getArtifacts()]
	    .filter(f => !!f);
      for (const fileToDelete of filesToDelete) {
	await removeFile(fileToDelete, {
	  reasonText: '',
	  missingText: '',
	  removeErrorText: ''
	});
      }
    }

    return undefined;
  }

  isValidWorker() {
    try {
      const gmVersion = graphicsmagickVersion().major;
      if (!['1'].includes(gmVersion)) {
	log.error(`Verify Imagemagick ${status.error}: Version 1 or greater expected. Saw version ${gmVersion}`);
	return false;
      }
    } catch (error) {
      if (/Command failed: convert -version/.test(error.message)) {
	return false;
      }
      throw error;
    }

    try {
      const ffVersion = ffmpegVersion().major.split('.')[0].replace(/[^0-9]/g, '');
      if (+ffVersion < 4) {
	log.error(`Verify Ffmepg ${status.error}: Version 4 or greater expected. Saw version ${ffVersion}`);
	return false;
      }
    } catch (error) {
      if (/Command failed: ffmpeg -version/.test(error.message)) {
	return false;
      }
      throw error;
    }
    return true;
  }

  getArtifacts() {
    return this.artifacts ?? [];
  }
}

export default DeepScanService;
