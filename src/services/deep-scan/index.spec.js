import fs from 'fs/promises';
import path from 'path';

import { afterAll, beforeEach, describe, expect, it, vi } from 'vitest';

import * as ffmpegModule from '../../common/ffmpeg/index.js';
import * as graphicsmagickModule from '../../common/graphicsmagick/index.js';
import Media from '../../entities/media/index.js';
import DeepScanService from '../../services/deep-scan/index.js';
import log from '../../services/log/index.js';
const TEST_ROOT = path.join(process.cwd(), '.test', 'deep-scan');
const GOOD_FILES_PATH = path.join(process.cwd(), './samples/test-good');
const EXT_FILES_PATH = path.join(process.cwd(), './samples/test-ext');

beforeEach(() => {
  vi.clearAllMocks();
  vi.spyOn(log, 'info').mockReturnValue(vi.fn());
  vi.spyOn(log, 'error').mockReturnValue(vi.fn());
  vi.spyOn(log, 'debug').mockReturnValue(vi.fn());
});

afterAll(async () => {
  await fs.rm(TEST_ROOT, { recursive: true, force: true });
});

const scanExamples = [
  {
    name: 'Basic usage',
    inputs: {
      file: path.join(GOOD_FILES_PATH, 'good.jpg'),
      options: { machineId: 'test-machine' }
    },
    want: {
      isMedia: true,
      hash: expect.any(String),
      size: expect.any(Number),
      type: 'jpg'
    }
  },
  {
    name: 'Invalid file',
    inputs: {
      file: path.join(EXT_FILES_PATH, 'txt.md'),
      options: { machineId: 'test-machine' }
    },
    want: {
      isMedia: false
    }
  }
];

describe('DeepScanService', () => {
  describe('scan', () => {
    scanExamples.forEach((example) => {
      it(example.name, async () => {
	const deepScanService = new DeepScanService();

        const result = await deepScanService.scan(example.inputs.file, example.inputs.options);

        if (example.want.isMedia) {
          expect(result).toBeInstanceOf(Media);
          expect(result.hash).toEqual(example.want.hash);
          expect(result.size).toEqual(example.want.size);
          expect(result.type).toEqual(example.want.type);
        } else {
          expect(result).toBeUndefined();
        }

        expect(log.debug).toHaveBeenCalled();
      });
    });
  });
});

const isValidWorkerExamples = [
  {
    name: 'Valid versions',
    setup: () => {
      vi.spyOn(graphicsmagickModule, 'version').mockReturnValue({ major: '1' });
      vi.spyOn(ffmpegModule, 'version').mockReturnValue({ major: '4.2.1' });
    },
    want: true
  },
  {
    name: 'Invalid Imagemagick version',
    setup: () => {
      vi.spyOn(graphicsmagickModule, 'version').mockReturnValue({ major: '0' });
      vi.spyOn(ffmpegModule, 'version').mockReturnValue({ major: '4.2.1' });
    },
    want: false
  },
  {
    name: 'Invalid Ffmpeg version',
    setup: () => {
      vi.spyOn(graphicsmagickModule, 'version').mockReturnValue({ major: '1' });
      vi.spyOn(ffmpegModule, 'version').mockReturnValue({ major: '3.2.1' });
    },
    want: false
  }
];

describe('isValidWorker', () => {
  isValidWorkerExamples.forEach((example) => {
    it(example.name, () => {
      example.setup();
      const deepScanService = new DeepScanService();
      const result = deepScanService.isValidWorker();
      expect(result).toBe(example.want);
    });
  });
});

describe('getArtifacts', () => {
  it('should return artifacts if they exist', async () => {
    const deepScanService = new DeepScanService();
    await deepScanService.scan(path.join(GOOD_FILES_PATH, 'good.jpg'));
    const artifacts = deepScanService.getArtifacts();
    expect(artifacts).toEqual(expect.arrayContaining([expect.stringContaining('_exiftool_tmp')]));
  });
});
