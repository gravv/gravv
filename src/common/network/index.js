import dns from 'dns';
import https from 'https';

import axios from 'axios';

import {
  hubUrl,
} from '../../constants/core/index.js';
import log from '../../services/log/index.js';
import {
  status
} from '../lib/index.js';

const hasDNS = () => new Promise((resolve, reject) => {
  dns.lookup('www.google.com', (err) => err ? reject(err) : resolve());
});

var hasHost = () => {
  return axios.create({
    baseURL: `${hubUrl}`,
    httpsAgent: new https.Agent({
      rejectUnauthorized: false,
    }),
  })
    .get(`/api/status`);
};

export const networkCheck = () => {
  return hasDNS()
    .then(() => log.debug(`Check internet connection ${status.complete}`))
    .catch(() => log.error(`Check internet connection ${status.error}: disconnected`));
};

export const serverCheck = () => {
  return hasHost()
    .then(() => log.debug(`Check api connection ${status.complete}`))
    .catch((error) => log.error(`Check api connection ${status.error}: ${error.message} (url: ${hubUrl}/api/status)`));
};
