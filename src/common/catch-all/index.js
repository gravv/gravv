import _ from 'lodash';
import statuses from 'statuses';

import { devEnvs } from '../../constants/system/index.js';

const omittedErrorProperties = ['code', 'expose', 'name', 'stack', 'statusCode'];

const send = (res, result) => {
  if (res.headersSent) {
    return false;
  }

  res.status(result.status).json(result);
  return true;
};

const sendError = (res, error) => {
  const status = error.status || 500;
  const result = {
    status,
    message: error.message,
    ..._.omit(error, omittedErrorProperties),
  };

  if (devEnvs.includes(process.env.NODE_ENV)) {
    result.stack = error.stack;
    result.stack = result.stack.split('\n');
  }

  return send(res, result);
};

const wrapAsync = (fn) => async (req, res, next) => {
  try {
    const result = await fn(req, res, next);

    if (res.headersSent) {
      return undefined;
    }

    if (result && statuses.redirect[result.status]) {
      res.redirect(result.status, result.redirectUrl);
    } else if (result) {
      send(res, result);
    }
  } catch (error) {
    if (!res.headersSent) {
      return sendError(res, error);
    }
    next(error);

  }
  return undefined;
};

const wrapAsyncWithoutSend = (fn) => async (req, res, next) => {
  try {
    await fn(req, res, next);
  } catch (error) {
    if (!res.headersSent) {
      return sendError(res, error);
    }
    next(error);
  }
  return undefined;
};


export default (fns) => {
  if (!Array.isArray(fns)) {
    if (typeof fns !== 'function') {
      throw new Error('fn is not a function');
    }
    return wrapAsync(fns);
  }

  const wrappedFns = fns.map((fn, index) => {
    if (typeof fn !== 'function') {
      throw new Error('fn is not a function');
    }
    return index === fns.length - 1 ? wrapAsync(fn) : wrapAsyncWithoutSend(fn);
  });

  return (req, res, next) => {
    (async () => {
      for (const fn of wrappedFns) {
        await fn(req, res, (error) => {
          if (error) throw error;
        });

	// don't attempt to run more middleware if errors
        if (res.headersSent) return;
      }
      next();
    })().catch(next);
  };
};
