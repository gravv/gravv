import childProcess from 'child_process';

import ffmpeg from 'fluent-ffmpeg';
import * as R from 'ramda';

import {
  bufferTypesAudio,
  bufferTypesVideo,
} from '../constants/files/index.js';
import { bufferType } from './files/index.js';

const sanitizeUnknown = (v) => v === 'unknown' ? undefined : v;

// based on: https://stackoverflow.com/a/63608717
// see: https://github.com/tibovanheule/bongo-converter/blob/master/index.js
export const collect = async file => {
  const ext = await bufferType(file);
  const isValid = [...bufferTypesVideo, ...bufferTypesAudio].includes(ext);
  if (!isValid) {
    return {};
  }

  let metadata;
  let errorFound;
  const cmd = ffmpeg();
  try {
    metadata = await (new Promise((resolve, reject) => {
      cmd
	.input(file)
	.ffprobe((error, data) => {
          if (error) {
            reject(error);
          }
          resolve(data);
	});
    }));
  } catch (error) {
    errorFound = error;
  }

  const streams = R.path(['streams'], metadata) || [];
  const format = R.path(['format'], metadata) || {};

  setTimeout(() => {
    cmd.kill();
  }, 10 * 60 * 1000);

  /*
   * Ffmpeg returns audio and video streams, but the order isn't consistent
   * So this picks the video stream,
   * which happens to return useful info for both videos and still images
   */
  const videoStream = streams
    .find(s => s.codec_type === 'video');

  const audioStream = streams
    .find(s => s.codec_type === 'audio');

  const subtitleStream = streams
    .find(s => s.codec_type === 'subtitle');

  const videoCodecName = R.path(['codec_name'], videoStream);
  const audioCodecName = R.path(['codec_name'], audioStream);
  const subtitleCodecName = R.path(['codec_name'], subtitleStream);

  const videoResult = videoStream ? {
    codecName: videoCodecName,
    colorSpace: R.path(['color_space'], videoStream),
    frameRate: R.path(['r_frame_rate'], videoStream),
    height: R.path(['height'], videoStream),
    width: R.path(['width'], videoStream),
  }: {};

  const audioResult = audioStream ? {
    audioBitRate: R.path(['bit_rate'], audioStream),
    audioChannelLayout: R.path(['channel_layout'], audioStream),
    audioChannels: R.path(['channels'], audioStream),
    audioCodecName,
    audioCodecTagString: R.path(['codec_tag_string'], audioStream),
    audioDuration: R.path(['duration'], audioStream),
    audioSampleFmt: R.path(['sample_fmt'], audioStream),
    audioSampleRate: R.path(['sample_rate'], audioStream),
    audioStartTime: R.path(['start_time'], audioStream),
    audioTimeBase: R.path(['time_base'], audioStream),
  } : {};

  let tags = '{}';
  let subtitleTags = '{}';
  try {
    tags = JSON.stringify(R.path(['tags'], format) || {}).slice(0, 200);
    subtitleTags = JSON.stringify(R.path(['tags'], subtitleStream) || {}).slice(0, 200);
  } catch (e) {}

  const subtitleResult = subtitleStream ? {
    subtitleCodecName,
    subtitleTags,
  } : {};

  const result = {
    ...videoResult,
    ...audioResult,
    ...subtitleResult,
    bitRate: R.path(['bit_rate'], format),
    duration: R.path(['duration'], format),
    formatName: R.path(['format_name'], format),
    nbStreams: R.path(['nb_streams'], format),
    probeScore: R.path(['probe_score'], format),
    size: R.path(['size'], format),
    tags,
    error: errorFound,
  };

  const resultSanitized = Object.keys(result).reduce((acc, key) => {
    acc[key] = sanitizeUnknown(result[key]);
    return acc;
  }, {});

  return resultSanitized;
};

// TODO Raspbian has an abnormal version string
export const version = () => {
  const linesString = childProcess.execSync('ffmpeg -version').toString();
  const lines = linesString.split('\n');
  const linesFirst = lines.length ? lines[0] : '';
  const versionRe = /.*(\d+)\.(\d+)\.(\d+)-([^\s]+).*/;
  const major = linesFirst.replace(versionRe, '$1');
  const minor = linesFirst.replace(versionRe, '$2');
  const patch = linesFirst.replace(versionRe, '$3');
  const hotfix = linesFirst.replace(versionRe, '$4');
  return { major, minor, patch, hotfix };
};
