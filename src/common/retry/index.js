import { setTimeout } from 'timers/promises';

import log from '../../services/log/index.js';

export default (fn, { maxRetries = 3, delay = 1000 }={}) => async (...args) => {
  let attempts = 0;
  while (attempts < maxRetries) {
    try {
      await fn(...args);
      return;
    } catch (error) {
      log.error(`Attempt ${attempts + 1} of ${maxRetries} failed: ${error.message} (Retrying in ${delay}ms.)`);
      if (['test', 'development'].includes(process.env.NODE_ENV)) {
	// eslint-disable-next-line
        console.error(error.stack);
      }
      attempts++;
      if (attempts >= maxRetries) {
	throw error;
      }
      await setTimeout(delay);
    }
  }
};
