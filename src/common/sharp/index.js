import sharp from 'sharp';

import {
  removeOnError,
} from '../../common/files/index.js';

const dimensions = (media) => {
  let result = { width: media.ffmpeg?.width, height: media.ffmpeg?.height };
  if (media.imagemagick?.width && media.imagemagick?.height) {
    result = { width: media.imagemagick.width, height: media.imagemagick.height };
  }
  return result;
};

export const scaleFactor = (media, targetLongSize=1) => {
  const { width, height } = dimensions(media);
  const wideDimension = Math.max(width, height);
  return targetLongSize / wideDimension;
};

export const toMimeExt = (ext) => {
  if (ext === 'jpg') {
    return 'jpeg';
  }
  // TODO mostly unimplemented
  return ext;
};

export const resize = async (media, image, resizeDef, ext, resizePath) => {
  return await removeOnError(async () => {
    const dimensionsFound = dimensions(media);
    const isWide = dimensionsFound.width > dimensionsFound.height;
    const width = isWide ? resizeDef.size : undefined;
    const height = !isWide ? resizeDef.size : undefined;
    await sharp(image)
      .resize(width, height)
      .timeout({ seconds: 20 })
      .toFormat(toMimeExt(ext))
    [toMimeExt(ext)]({
          ...resizeDef,
      force: true,
    })
      .toFile(resizePath);
    return resizePath;
  }, [resizePath]);
};
