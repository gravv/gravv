import childProcess from 'child_process';

import gm from 'gm';
import * as R from 'ramda';

import {
  removeOnError,
} from '../../common/files/index.js';
import {
  bufferTypesImage,
  imagemagickProperties,
} from '../../constants/files/index.js';
import { bufferType } from '../files/index.js';

const sanitizeUndefined = (v) => v === 'Undefined' ? undefined : v;

export const convert = async (media, outputPath) => {
  let filePrefixed = media.exif.SourceFile;
  if (media.type === 'dng') {
    filePrefixed = `dng:${media.exif.SourceFile}`;
  }
  return await removeOnError(async () => {
    return await new Promise((resolve, reject) => {
      gm(filePrefixed)
        .quality(100)
        .write(outputPath, error => {
          if (error) reject(error);
          resolve(outputPath);
        });
    });
  }, [outputPath]);
};

// based on https://github.com/mikespencer/resume/blob/master/scripts/compare.js
// assumes version
export const collect = async file => {
  const ext = await bufferType(file);
  const isValid = bufferTypesImage.includes(ext);
  if (!isValid) {
    return {};
  }

  let metadata;
  let error;
  try {
    metadata = await (new Promise((resolve, reject) => {
      gm(file).identify((err, res) => {
        if (err) {
          return reject(err.message);
        }
        resolve(res);

	return undefined;
      });
    }));
  } catch (e) {
    // swallow signature errors
    if (!/Command failed/.test(e.message)) {
      error = e.message;
    }
  }

  const result = {
    entropy: R.path(['Image statistics', 'Overall', 'entropy'], metadata),
    height: R.path(['size', 'height'], metadata),
    kurtosis: R.path(['Image statistics', 'Overall', 'kurtosis'], metadata),
    mean: R.path(['Image statistics', 'Overall', 'mean'], metadata),
    signature: R.path(['Properties', 'signature'], metadata),
    skewness: R.path(['Image statistics', 'Overall', 'skewness'], metadata),
    transparentColor: R.path(['Transparent color']),
    width: R.path(['size', 'width'], metadata),
    ...R.pick(imagemagickProperties, (metadata || {})),
    error,
  };

  const resultSanitized = Object.keys(result).reduce((acc, key) => {
    acc[key] = sanitizeUndefined(result[key]);
    return acc;
  }, {});

  return resultSanitized;
};

// TODO determine if these are guaranteed to be integers
export const version = () => {
  const linesString = childProcess.execSync('gm convert -version').toString();
  const lines = linesString.split('\n');
  const linesFirst = lines.length ? lines[0] : '';
  const versionRe = /GraphicsMagick\s(\d+)\.(\d+).*/;
  const major = linesFirst.replace(versionRe, '$1');
  const minor = linesFirst.replace(versionRe, '$2');
  return { major, minor};
};
