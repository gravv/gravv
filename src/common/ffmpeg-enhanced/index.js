import ffmpeg from 'fluent-ffmpeg';

import log from '../../services/log/index.js';

const granularityLevels = [
  { name: '10min', seconds: 600 },
  { name: '1min', seconds: 60 },
  { name: '10s', seconds: 10 },
  { name: '1s', seconds: 1 },
  { name: '0.1s', seconds: 0.1 }
];

function getVideoDuration(metadata, startTime, endTime) {
  const duration = metadata.format.duration;
  const start = startTime || 0;
  const end = endTime || duration;
  return end - start;
}

function selectGranularity(videoDuration, interval, maxFrames) {
  if (interval) {
    const frameCount = Math.floor(videoDuration / (interval / 1000));
    return {
      granularity: granularityLevels.find(level => level.seconds <= interval / 1000),
      frameCount
    };
  } else {
    const frameCount = maxFrames || 10;
    for (const level of granularityLevels) {
      const possibleFrameCount = Math.floor(videoDuration / level.seconds);
      if (possibleFrameCount <= frameCount) {
        return { granularity: level, frameCount: possibleFrameCount };
      }
    }
  }
  return { granularity: null, frameCount: 0 };
}

function buildOutputOptions(frameInterval, frameCount, avgFrameRate) {
  return [
    '-vf', `select='not(mod(n,${Math.round(frameInterval * avgFrameRate)}))`,
    '-vsync', 'vfr',
    '-q:v', '2',
    '-frames:v', frameCount
  ];
}

function buildInputOptions(startTime, endTime) {
  const options = [];
  if (startTime) options.push('-ss', startTime);
  if (endTime) options.push('-to', endTime);
  return options;
}

function generateFilePaths(outputFolder, frameCount) {
  return Array.from(
    { length: frameCount },
    (_, i) => `${outputFolder}/frame${i + 1}.jpg`
  );
}

const enhancedFfmpeg = new Proxy(ffmpeg, {
  apply(target, thisArg, args) {
    const cmd = target();
    let inputSet = false;

    if (args.length > 0) {
      cmd.input(args[0]);
      inputSet = true;
    }

    return new Proxy(cmd, {
      get(cmdTarget, prop) {
        if (prop === 'resize') {
          return function(options) {
            const { width, height, bitrate, truncateAt } = options;

            let sizeStr = `${width}x${height}`;
            if (!width || !height) {
              sizeStr = `${width || '?'}x${height || '?'}`;
            }

            cmdTarget.size(sizeStr);

            const outputOptions = [
              '-c:v', 'libx264',
              '-c:a', 'copy',
              '-preset', 'slow',
              '-movflags', '+faststart'
            ];

            if (bitrate) {
              outputOptions.push('-b:v', `${bitrate}M`);
            }

            if (truncateAt) {
              outputOptions.push('-t', truncateAt);
            }

            cmdTarget.outputOptions(outputOptions);
            return cmdTarget;
          };
        }

        if (prop === 'extractCover') {
          return function(options = {}) {
            const { offset = 0 } = options;
            cmdTarget
              .inputOptions(['-ss', offset / 1000, '-noaccurate_seek'])
              .outputOptions(['-vframes', 1, '-q:v', 1]);
            return this;
          };
        }

        if (prop === 'extractFrames') {
          return function(options) {
            const { count, interval, offset = 0 } = options;
            cmdTarget
              .inputOptions(['-ss', offset / 1000])
              .outputOptions([
                '-vf', `select='not(mod(n,${interval}))',setpts=N/FRAME_RATE/TB`,
                '-frames:v', count
              ]);
            return this;
          };
        }

        if (prop === 'addSubtitles') {
          return function(options) {
            const { subtitlePath } = options;
            cmdTarget
              .outputOptions([
                '-vf', `subtitles=${subtitlePath}`
              ]);
            return this;
          };
        }

        if (prop === 'advancedExtractFrames') {
          return function(options) {
            const {
              interval,
              maxFrames,
              startTime,
              endTime,
              outputFolder
            } = options;

            return new Promise((resolve, reject) => {
              cmdTarget.ffprobe((err, metadata) => {
                if (err) {
                  reject(err);
                  return;
                }

                const videoDuration = getVideoDuration(metadata, startTime, endTime);
                const { granularity, frameCount } = selectGranularity(videoDuration, interval, maxFrames);

                if (!granularity) {
                  reject(new Error('Frame budget is not sufficient for the video duration'));
                  return;
                }

                const frameInterval = granularity.seconds;
                const avgFrameRate = metadata.streams[0].avg_frame_rate;
                const outputOptions = buildOutputOptions(frameInterval, frameCount, avgFrameRate);
                const inputOptions = buildInputOptions(startTime, endTime);

                const outputPattern = `${outputFolder}/frame%d.jpg`;

                cmdTarget
                  .inputOptions(inputOptions)
                  .outputOptions(outputOptions)
                  .output(outputPattern)
                  .on('end', () => {
                    const filePaths = generateFilePaths(outputFolder, frameCount);
                    resolve(filePaths);
                  })
                  .on('error', reject)
                  .run();
              });
            });
          };
        }

        if (prop === 'run') {
          return function() {
            return new Promise((resolve, reject) => {
              if (!inputSet) {
                reject(new Error('No input specified'));
                return;
              }

	      cmdTarget
		    .on('start', (commandLine) => {
		      log.debug(`Spawned FFmpeg with command: ${commandLine}`);
		    })
		    .on('stderr', (stderrLine) => {
		      log.debug(`FFmpeg debug output: ${stderrLine}`);
		    })
		    .on('end', async () => {
		      log.debug('FFmpeg [complete]');

		      resolve();
		    })
		    .on('error', (err) => {
		      log.error('FFmpeg encountered an error:', err);
		      reject(err);
		    })
		    .run();
            });
          };
        }

        // Ensure all other methods stay chained to the proxy
        if (typeof cmdTarget[prop] === 'function') {
          return function(...cmdArgs) {
            const result = cmdTarget[prop](...cmdArgs);
            return result === cmdTarget ? this : result;  // Return the proxy if method is chainable
          };
        }

        // If it's not a function, return it directly
        return cmdTarget[prop];
      }
    });
  }
});

export default enhancedFfmpeg;
