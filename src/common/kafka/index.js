import Kafka from 'kafkajs';

import {
  brokers,
  clientId,
  initialRetryTime,
  retries,
} from '../../constants/core/index.js';
import logService from '../../services/log/index.js';

export const makeClient = (config = {}) => new Kafka.Kafka({
  brokers, // ie. ['kafka:9092'],
  clientId, // ie. 'localhost'
  connectionTimeout: 5000,
  retry: {
    initialRetryTime,
    retries,
  },
  logCreator: () => ({ level, log }) => {
    const { message, ...extra } = log || {};

    if (level === Kafka.logLevel.ERROR) {
      logService.error(message, extra);
    } else if (level === Kafka.logLevel.WARN) {
      logService.warn(message, extra);
    } else if (level === Kafka.logLevel.INFO) {
      logService.info(message, extra);
    } else if (level === Kafka.logLevel.DEBUG) {
      logService.debug(message, extra);
    }
  },
  ...config,
});

// producer.send(msg)
// { topic: 'abc', messages: [{ key: 'xyz', value: 'updated' }]}
// { topic: 'abc', messages: [{ key: 'xyz', value: JSON.stringify(foo) }]}
// TODO can error upon creation
export const makeProducer = (client, config = {}) => {
  const producer = client.producer(config);

  const { DISCONNECT, CONNECT } = producer.events;
  producer.on(DISCONNECT, async () => {
    logService.info('Kafka producer is disconnected');
    await producer.connect();
  });
  producer.on(CONNECT, async () => {
    logService.info('Kafka producer is connected');
  });

  return producer;
};

export const listTopics = async (client) => {
  const topics = await client.fetchTopicMetadata();
  return topics;
};

export const listTopicsAlt = async (client) => {
  return await client.fetchTopicMetadata();
};

export const createTopicAlt = async (name, admin) => {
  const topics = [{
    topic: name,
    numPartitions: 1,
    replicationFactor: 1,
  }];

  await admin.createTopics({ topics });
};

export const makeGroupId = () => Math.round(Math.random() * 1e10).toString(36).slice(0, 7);

// https://kafka.js.org/docs/1.10.0/consuming
export const makeConsumer = async (topic, client, {
  fromBeginning = false,
  groupId,
  requestTimeout=60000
} = {}) => {
  const groupIdResolved = groupId || makeGroupId();

  // Kafka consumer group is a collection of Kafka consumers who can read data in parallel from a Kafka topic.
  // Should have enough consumers of a group to handle to partitions
  const consumer = client.consumer({
    groupId: groupIdResolved,
    allowAutoTopicCreation: false,
    requestTimeout
  });

  await consumer.connect();

  await consumer.subscribe({ fromBeginning, topic });

  return consumer;
};

export const hasAllTopics = async (topicConfigsNeeded, { client }) => {
  const admin = client.admin();
  await admin.connect();

  const topicsNeeded = topicConfigsNeeded.map(config => config.topic);

  const topicsHave = await admin.listTopics();

  await admin.disconnect();

  return !topicsNeeded.some(t => !topicsHave.includes(t));
};

export const setupTopics = async (topicConfigsNeeded = [], { client }) => {
  if (!topicConfigsNeeded.length) {
    throw new Error('No topic configurations provided.');
  }

  const admin = client.admin();
  await admin.connect();
  let missingTopics = [];

  try {
    const topicsNeeded = topicConfigsNeeded.map(config => config.topic);
    const topicsHave = await admin.listTopics();

    missingTopics = topicsNeeded.filter(t => !topicsHave.includes(t));

    if (missingTopics.length > 0) {
      await admin.createTopics({
        timeout: 30 * 1000,
        topics: topicConfigsNeeded,
      });
      logService.debug(`Created missing topics: ${missingTopics.join(', ')}`);
    } else {
      logService.debug('All required topics already exist.');
    }
  } catch (error) {
    throw new Error(`Error setting up topics: ${error.message}`);
  } finally {
    await admin.disconnect();
  }

  return !!missingTopics.length; // true if created
};
