import childProcess from 'child_process';
import util from 'util';

import { sanitizeShellFilename } from '../files/index.js';

const exec = util.promisify(childProcess.exec);

export const sha256Sum = async (path) => {
  const { stdout } = await exec(`sha256sum ${sanitizeShellFilename(path)}`);
  if (stdout) {
    return stdout.toString().split(' ')[0];
  }
  throw new Error('Processing error: No sha256 output');
};
