import fsCb from 'fs';
import path from 'path';

import chokidar from 'chokidar';
import fileType from 'file-type';
import glob from 'glob';
import * as R from 'ramda';
import through from 'through2';

import { status } from '../../common/lib/index.js';
import {
  fileWaitMs,
  fileWaitPollMs,
} from '../../constants/core/index.js';
import {
  bufferTypesImage,
  bufferTypesVideo,
  bufferTypesSupported,
  sharpTypes,
  targetVideoType,
  targetImageType,
} from '../../constants/files/index.js';
import log from '../../services/log/index.js';

const fs = fsCb.promises;

export const bufferType = async (file) => {
  const fileTypeResult = await fileType.fromFile(file);
  return fileTypeResult ? fileTypeResult.ext : undefined;
};

// From https://github.com/danseethaler/dayone-import-wizard/blob/42565b86ad1bb287d88313267d611c64c4888284/src/services/convert/processFile.js
// https://github.com/sindresorhus/filename-reserved-regex/blob/master/i// https://github.com/preichelt/lftp/blob/master/dist/lftp.js#L95
export const sanitizeShellFilename = (file) =>
  file
    .replace(/['[\]{}()*+?.,\\^$|#\s]/g, '\\$&');

// From https://remarkablemark.org/blog/2017/12/17/touch-file-nodejs/
export const touch = async (file) => {
  const time = new Date();
  try {
    await fs.utimes(file, time, time);
  } catch (e) {
    const opened = await fs.open(file, 'w');
    await opened.close();
  }
};

// From: https://stackoverflow.com/a/57708635
export const exists = async (path) => {
  return !!(await fs.stat(path).catch(() => false));
};

/**
 * Validate that a file can be processed.
 * Less strict than validateFile for gravv. In particular, extension types are not checked.
 */
export const isEmpty = async (file) => {
  const isExists = await exists(file);
  let fileStat;
  try {
    fileStat = await fs.lstat(file);
  } catch (e) {
    return true;
  }
  return !isExists ||
    fileStat.isDirectory() ||
    fileStat.size === 0;
};

export const stat = async (path) => {
  const stat = await fs.stat(path);

  return {
    atime: stat.atime,
    birthtime: stat.birthtime,
    ctime: stat.ctime,
    gid: stat.gid,
    isDirectory:stat.isDirectory(),
    isFile: stat.isFile(),
    isSymbolicLink: stat.isSymbolicLink(),
    mode: stat.mode,
    mtime: stat.mtime,
    size: stat.size,
    uid: stat.uid,
  };
};

export const statReduced = (path) => R.pick([
  'atime',
  'birthtime',
  'ctime',
  'mtime',
  'size',
], stat(path));

export const collectDir = (globDir) => {
  return (new Promise((resolve, reject) => {
    const g = new glob.Glob(globDir, { nodir: true });
    const files = [];
    g.on('match', async (file) => {
      files.push(file);
    });
    g.on('end', () => {
      resolve(files);
    });
    g.on('error', (error) => {
      reject(error);
    });
  }));
};

export const createDir = async (fileOrPath) => {
  const dirName = fileOrPath;

  const dirExists = await exists(dirName);

  if (!dirExists) {
    try {
      await fs.mkdir(dirName, { recursive: true });
      log.info(`Directory create ${status.created}: Directory created (dir: ${dirName})`);
    } catch (error) {
      if (error.code === 'EACCES') {
	throw error;
      }
      log.info(`Directory create ${status.error}: Directory not found. ${error.message} (dir: ${dirName})`);
    }
  } else {
    const files = await collectDir(`${dirName}/**/*`);
    log.info(`Directory create: Found ${files.length} files (dir: ${dirName})`);
  }

  log.info(`Directory create ${status.complete} (dir: ${dirName})`);
};

export const removeFile = async (file, { reasonText, missingText, removeErrorText, logLevel='DEBUG' }={}) => {
  try {
    if (reasonText) {
      log[logLevel.toLowerCase()](reasonText);
    }

    await fs.unlink(file);
  } catch (e) {
    if (e.code === 'ENOENT') {
      if (missingText) {
	log.info(missingText);
      }

      return;
    }

    if (removeErrorText) {
      log[logLevel.toLowerCase()](`${removeErrorText} [${e.message}]`);
    }

    // do nothing
  }
};

/**
 * Recursively removes empty directories from the given directory.
 *
 * If the directory itself is empty, it is also removed.
 *
 * Code taken from: https://gist.github.com/fixpunkt/fe32afe14fbab99d9feb4e8da7268445
 *
 * @param {string} directory Path to the directory to clean up
 */
export const removeEmptyDirectories = async (directory, { removeRoot=false }={}) => {
  // lstat does not follow symlinks (in contrast to stat)
  const fileStats = await fs.lstat(directory);
  if (!fileStats.isDirectory()) {
    return;
  }
  let fileNames = await fs.readdir(directory);
  if (fileNames.length > 0) {
    const recursiveRemovalPromises = fileNames.map(
      (fileName) => removeEmptyDirectories(path.resolve(directory, fileName)),
    );
    await Promise.all(recursiveRemovalPromises);

    // re-evaluate fileNames; after deleting subdirectory
    // we may have parent directory empty now
    fileNames = await fs.readdir(directory);
  }

  if (fileNames.length === 0 && removeRoot) {
    await fs.rmdir(directory);
  }
};

const ms1Mins = 1000 * 60;

export const setIntervalAlt = async (fn, ms, config = {}) => {
  if (config.leading) {
    await fn();
  }
  return setInterval(fn, ms);
};

export const watchDirRestartable = (
  watchDir,
  {
    cbComplete = () => null,
    cbStart = () => null,
    cbError = () => null,
    intervalMs = 30 * ms1Mins,
  },
) => {
  const fileStream = through.obj((item, enc, cb) => { cb(null, item); });
  let watcher;
  let isProcessing = false;

  const startWatcher = () => {
    if (watcher) {
      return;
    }

    cbStart();

    watcher = chokidar.watch(`${watchDir}/**/*`, {
      persistent: true,
      // suppress the add event until filesize stops changing
      awaitWriteFinish: {
        stabilityThreshold: fileWaitMs,
        pollInterval: fileWaitPollMs,
      },
      ignorePermissionErrors: true,
    });

    watcher
      .on('add', handleFileEvent)
      .on('change', handleFileEvent)
      .on('ready', cbComplete)
      .on('error', cbError);
  };

  const handleFileEvent = (filepath) => {
    fileStream.write(filepath);
  };

  const processExistingFiles = async () => {
    if (isProcessing) {
      return;
    }

    isProcessing = true;

    try {
      const files = await getAllFiles(watchDir);
      files.map((file) => fileStream.write(file));
    } finally {
      isProcessing = false;
    }
  };

  const getAllFiles = async (dir) => {
    const entries = await fs.readdir(dir, { withFileTypes: true });
    const files = await Promise.all(entries.map((entry) => {
      const res = path.resolve(dir, entry.name);
      return entry.isDirectory() ? getAllFiles(res) : res;
    }));
    return files.flat();
  };

  // Start the watcher immediately
  startWatcher();

  // Set up polling interval
  const intervalId = setInterval(processExistingFiles, intervalMs);

  return {
    fileStream,
    intervalId,
    close: async () => {
      clearInterval(intervalId);
      if (watcher) {
        await watcher.close();
        watcher = null;
      }
      fileStream.end();
    }
  };
};

export const removeOnError = async (invoker, filenames = []) => {
  try {
    const result = await invoker();
    return result;
  } catch (error) {
    await Promise.all(filenames.map(async file => {
      if (await exists(file)) {
	await removeFile(file, {
	  reasonText: `Removing file due to operation failure: ${file}`,
	  missingText: `File not found, nothing to remove: ${file}`,
	  removeErrorText: `Failed to remove file: ${file}`,
	});
      }
    }));
    throw error;
  }
};

export const loadNdJson = (file) => {
  const out = [];
  return new Promise((resolve, reject) => {
    fsCb.createReadStream(file)
      .pipe(ndjson.parse())
      .on('data', (obj) => {
        out.push(obj);
      })
      .on('error', (error) => {
        reject(error);
      })
      .on('finish', () => {
        resolve(out);
      });
  });
};

export const extTarget = (ext) => {
  const isVideo = bufferTypesVideo.includes(ext);
  const isImage = bufferTypesImage.includes(ext);
  const isSupported = bufferTypesSupported.includes(ext);
  const isSharpType = sharpTypes.includes(ext);

  let target = ext;
  if (isVideo) {
    target = isSupported ? ext : targetVideoType;
  } else if (isImage) {
    target =  (isSupported && isSharpType) ? ext : targetImageType;
  }

  return target;
};
