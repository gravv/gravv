import log from '../../../services/log/index.js';

const onShutdown = (cb) => {
  const errorTypes = ['unhandledRejection', 'uncaughtException'];
  const signalTraps = ['SIGTERM', 'SIGINT', 'SIGUSR2'];

  errorTypes.map(type => {
    process.on(type, async (error) => {
      if (['test', 'development'].includes(process.env.NODE_ENV)) {
	// eslint-disable-next-line
	console.error(error);
      }
      log.error(`${type} error: ${error.message}`);
      try {
	await cb();
      } catch (shutdownError) {
	log.error(`Shutdown error during ${type} error: ${error.message}`);
	if (['test', 'development'].includes(process.env.NODE_ENV)) {
	  throw shutdownError;
	}
      } finally {
	process.exit(1);
      }
    });
  });

  signalTraps.map(type => {
    process.once(type, async () => {
      try {
	await cb();
	process.exit(0);
      } catch (shutdownError) {
	log.error(`Shutdown error during ${type} error: ${error.message}`);
	if (['test', 'development'].includes(process.env.NODE_ENV)) {
	  throw shutdownError;
	}
      } finally {
	process.kill(process.pid, type);
      }
    });
  });
};

export default onShutdown;
