import fs from 'fs/promises';
import path from 'path';
import { fileURLToPath } from 'url';

import baseX from 'base-x';
import * as R from 'ramda';
import upperCaseFirstModule from 'upper-case-first';


const upperCaseFirst = upperCaseFirstModule.upperCaseFirst;

export const configDirEnvironment = `-${process.env.NODE_ENV}`;

export const random = ([min, max]) => min + Math.random() * (max - min);

export const convertBase = (str, fromBase, toBase) => {
  const DIGITS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+/';
  const decoder = baseX(DIGITS.slice(0, fromBase));
  const encoder = baseX(DIGITS.slice(0, toBase));
  return encoder.encode(decoder.decode(str));
};

export const prefixName = (prefix, property) =>
  `${prefix}${upperCaseFirst(property)}`;

export const namespace = (item, prefix) =>
  Object.keys(item).reduce(
    (memo, key) => ({
      ...memo,
      [prefixName(prefix, key)]: item[key],
    }),
    {}
  );

export const applyTemplate = (obj, template) => {
  const matches = template.match(/{[^}]+}/g);
  return matches.reduce((result, match) =>
    result.replace(`${match}`, obj[match.slice(1, -1)]),
  template);
};

export const resolve = (file) =>
  file[0] === '~' ?
    path.join(process.env.HOME, file.slice(1)) :
    path.resolve(file);

export const status = {
  complete: `[complete]`,
  created: `[created]`,
  debug: `[debug]`,
  error: `[error]`,
  skipped: `[skipped]`,
  started: `[started]`,
  removed: `[removed]`,
  warning: `[warning]`,
};

export const resolveRoot = (file='.') => {
  const __filename = fileURLToPath(import.meta.url);
  return path.join(__filename, '../../../../', file);
};

export const falsyValues = ['false', '0'];

export const createJSONFileIfNotExists = async (file, data) => {
  try {
    const fileData = await fs.readFile(file, 'utf8');

    return JSON.parse(fileData);
  } catch (error) {
    if (error.code === 'ENOENT') {
      // File does not exist, create it
      await fs.mkdir(path.dirname(file), { recursive: true });
      await fs.writeFile(file, JSON.stringify(data, null, 2), 'utf8');
      return data;
    } else {
      throw error;
    }
  }
};

export const earliestDate = (possibleDates=[], { minStartDate }={}) => {
  const fudgeFactorMs = 24 * 60 * 60 * 1000; // 24-hour delta in milliseconds
  const now = new Date();
  const maxValidDate = new Date(now.getTime() + fudgeFactorMs);
  const minValidDate = minStartDate ? new Date(minStartDate) : new Date(0);

  const possibleDatesValidOnly = possibleDates
    .map(val => new Date(val))
    .filter(d => d.toString() !== 'Invalid Date')
    .filter(d => minValidDate <= d && d <= maxValidDate)
    .map(d => +d);

  const found = Math.min(...possibleDatesValidOnly);
  return found ? new Date(found) : undefined;
};

export const toNearestMinute = (value) => {
  return Math.round(value / (1000 * 60)) * (1000 * 60);
};

export const toSlug = (obj) => {
  const entries = Object.entries(obj);
  const sorted = R.sort((a, b) => a[0].localeCompare(b[0]), entries);
  const mapped = sorted
    .map(([a, b]) => `${(a + '').replace(/\./g, 'p')}-${(b + '').replace(/\./g, 'p')}`);
  return mapped.join('-');
};
