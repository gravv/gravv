import exiftoolImport from 'exiftool-vendored';
import geotz from 'geo-tz';

import log from '../../services/log/index.js';

const ExifTool = exiftoolImport.ExifTool;

const exifDates = [
  'CreateDate',
  'DateTimeOriginal',
  'FileAccessDate',
  'FileModifyDate',
  'ModifyDate',
];

export const exif = async (path) => {
  let res = {};
  const exiftool = new ExifTool({
    taskTimeoutMillis: 60000,
    maxProcs: 2,
    minDelayBetweenSpawnMillis: 0,
    geoTz: (lat, lon) => geotz.find(lat, lon)[0],
    // readArgs: ['-overwrite_original']
  });
  let errorFound;
  try {
    res = await exiftool.read(path);
  } catch (error) {
    errorFound = error;
    log.error(`Exif error [${error}]: ${error.message}`, error);
  } finally {
    exiftool.end();
  }

  if (errorFound) throw errorFound;

  return res;
};

const parseExifDate = (value) => {
  if (value && typeof value === 'object' && value.rawValue) {
    // Parse the rawValue to extract the date components
    const match = value.rawValue.match(/^(\d{4}):(\d{2}):(\d{2}) (\d{2}):(\d{2}):(\d{2})([+-]\d{2}:\d{2})?$/);
    if (match) {
      const [ , year, month, day, hour, minute, second] = match;
      return `${year}-${month}-${day}T${hour}:${minute}:${second}`;
    }
    return value.rawValue;
  }
  return value;
};

export const exifParseDates = (data = {}) => {
  const dataFixed = Object.keys(data).reduce((acc, key) => {
    if (exifDates.includes(key)) {
      acc[key] = parseExifDate(data[key]);
    } else {
      acc[key] = data[key];
    }
    return acc;
  }, {});

  if (dataFixed.GPSDateStamp) {
    const gpsDate = dataFixed.GPSDateStamp.replace(/:/g, '-');
    const gpsTime = dataFixed.GPSTimeStamp;
    const gpsTimeRounded = gpsTime ?? '12:00:00';

    data.GPSDate = `${gpsDate}T${gpsTimeRounded}`;
    delete data.GPSDateStamp;
    delete data.GPSTimeStamp;
  }

  return dataFixed;
};

export const exifParseExt = (data = {}) => {
  const result = { ...data };
  if (result.FileType) {
    result.FileType = result.FileType.toLowerCase();
  }
  if (result.GPSLatitude === 0 && result.GPSLongitude === 0) {
    delete result.GPSLatitude;
    delete result.GPSLongitude;
  }
  if (result.Author === '') {
    delete result.Author;
  }
  if (result.Title === '') {
    delete result.Title;
  }
  if (result.ResolutionUnit === 'None') {
    delete result.ResolutionUnit;
  }
  delete result.FileInodeChangeDate;

  return result;
};

export const exifReduced = async (file) => {
  const result = await exif(file);
  const resultParseDates = exifParseDates(result);
  const resultParseExt = exifParseExt(resultParseDates);
  return resultParseExt;
};

export const loadAndRepairExif = async (
  file
) => {
  return await exifReduced(file);
};
