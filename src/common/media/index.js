import fsCb from 'fs';

import axios from 'axios';
import * as R from 'ramda';

import {
  hubUrl,
} from '../../constants/core/index.js';
import {
  bufferTypesSupported,
  frequentlyChangingProperties,
  ignorePatterns,
} from '../../constants/files/index.js';
import {
  bufferType,
  exists,
} from '../files/index.js';

const fs = fsCb.promises;

export const validateFile = async (file) => {
  const isExists = await exists(file);
  const fileBufferType = await bufferType(file);
  const fileStat = await fs.lstat(file);
  const isIgnored = ignorePatterns.some(pattern => ((new RegExp(pattern)).test(file)));
  const isBufferValid = bufferTypesSupported.includes(fileBufferType);
  return !isIgnored &&
    isExists &&
    isBufferValid &&
    !fileStat.isDirectory() &&
    fileStat.size > 0;
};
