import log from '../../services/log/index.js';

export const getValueFromPath = (obj, path) => {
  try {
    return path.split('.')
      .reduce((acc, key) => acc[key], obj);
  } catch (error) {
    log.error(`Path split error: ${error.message} (keys: ${Object.keys(obj)}, path: ${path})`);

    throw error;
  }
};
