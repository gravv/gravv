import Artifacts from '../../entities/artifacts/index.js';
import Notes from '../../entities/notes/index.js';
import Snippets from '../../entities/snippets/index.js';
import Exif from '../../values/exif-data/index.js';
import Ffmpeg from '../../values/ffmpeg-data/index.js';
import Identity from '../../values/identity-data/index.js';
import Imagemagick from '../../values/imagemagick-data/index.js';
import Stat from '../../values/stat-data/index.js';

class Media {
  constructor(data = {}) {
    this.filename = data.filename;
    this.hash = data.hash;
    this.size = data.size;
    this.type = data.type;
    this.createdAt = data.createdAt;
    this.modifiedAt = data.modifiedAt;
    this.deletedAt = data.deletedAt;

    this.exif = data.exif instanceof Exif ? data.exif : new Exif(data.exif);
    this.ffmpeg = data.ffmpeg instanceof Ffmpeg ? data.ffmpeg : new Ffmpeg(data.ffmpeg);
    this.identity = data.identity instanceof Identity ? data.identity : new Identity(data.identity);
    this.imagemagick = data.imagemagick instanceof Imagemagick ? data.imagemagick : new Imagemagick(data.imagemagick);
    this.stat = data.stat instanceof Stat ? data.stat : new Stat(data.stat);
    this.artifacts = data.artifacts instanceof Artifacts ? data.artifacts : new Artifacts(data.artifacts);
    this.snippets = data.snippets instanceof Snippets ? data.snippets : new Snippets(data.snippets);
    this.notes = data.notes instanceof Notes ? data.notes : new Notes(data.notes);
  }

  getArtifact(queryTags = [], preferredSize = 'large') {
    const isImageRequest = queryTags.includes('image');
    const matchingArtifacts = this.artifacts.filter(a =>
      queryTags.every(tag => a.tags.includes(tag))
    );

    if (matchingArtifacts.length === 0) return null;

    const findArtifact = (role, size) =>
	  matchingArtifacts.find(a => a.tags.includes(`role:${role}`) && a.tags.includes(`size:${size}`));

    const findArtifactBySize = (size) =>
	  matchingArtifacts.find(a => a.tags.includes(`size:${size}`));

    // For video, handle image requests specially
    if (this.type === 'video' && isImageRequest) {
      return findArtifact('cover', preferredSize) ||
        findArtifact('cover', 'large') ||
        findArtifact('cover', 'small');
    }

    // For all other cases, prefer the specified size with 'base' role
    const artifactFound = findArtifact('base', preferredSize) ||
          findArtifact('base', 'large') ||
          findArtifact('base', 'small');

    // If no 'base' role artifact is found, try finding by size without role
    if (artifactFound) return artifactFound;

    return findArtifactBySize(preferredSize) ||
      findArtifactBySize('large') ||
      findArtifactBySize('medium') ||
      findArtifactBySize('small') ||
      matchingArtifacts[0];
  }

  toJSON(config) {
    return {
      filename: this.filename,
      hash: this.hash,
      size: this.size,
      type: this.type,
      createdAt: this.createdAt,
      modifiedAt: this.modifiedAt,
      deletedAt: this.deletedAt,
      exif: this.exif.toJSON(config),
      ffmpeg: this.ffmpeg.toJSON(config),
      identity: this.identity.toJSON(config),
      imagemagick: this.imagemagick.toJSON(config),
      stat: this.stat.toJSON(config),
      artifacts: this.artifacts.toJSON(config),
      snippets: this.snippets.toJSON(config),
      notes: this.notes.toJSON(config),
    };
  }

  static merge(existingMedia, incomingMedia) {
    const merged = new Media(existingMedia);
    Object.entries(incomingMedia).forEach(([key, value]) => {
      if (merged[key] && typeof merged[key].merge === 'function') {
        merged[key] = merged[key].merge(value);
      } else if (value !== undefined) {
        merged[key] = value;
      }
    });
    return merged;
  }

  addArtifact(artifact) {
    this.artifacts.add(artifact);
  }

  addSnippet(snippet) {
    this.snippets.add(snippet);
  }

  addNote(note) {
    this.notes.add(note);
  }

  static fromJSON(data) {
    return new Media(data);
  }
}

export default Media;
