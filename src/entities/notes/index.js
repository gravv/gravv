import BaseCollection from '../../entities/base-collection/index.js';

export default class Notes extends BaseCollection {
  merge(otherNotes) {
    otherNotes.toArray().forEach(note => this.add(note));
    return this;
  }

  all() {
    return this.toArray();
  }
}
