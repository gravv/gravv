export default class BaseCollection {
  constructor(items = []) {
    let itemsResolved = items;
    if (!items.length) {
      itemsResolved = Object.values(items);
    }
    this._items = new Set(itemsResolved);
  }

  add(item) {
    this._items.add(item);
  }

  has(item) {
    return this._items.has(item);
  }

  delete(item) {
    return this._items.delete(item);
  }

  clear() {
    this._items.clear();
  }

  size() {
    return this._items.size;
  }

  toArray() {
    return Array.from(this._items);
  }

  toJSON({ collections }={}) {
    if (collections === 'keyValue') {
      return this.toArray().reduce((result, item) => {
	if (item.name) {
	  result[item.name] = item;
	}
	return result;
      }, {});
    }
    return this.toArray();
  }

  [Symbol.iterator]() {
    return this._items[Symbol.iterator]();
  }
}
