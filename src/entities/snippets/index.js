import BaseCollection from '../../entities/base-collection/index.js';

export default class Snippets extends BaseCollection {
  merge(otherSnippets) {
    otherSnippets.toArray().forEach(snippet => this.add(snippet));
    return this;
  }

  all() {
    return this.toArray();
  }
}
