import BaseCollection from '../../entities/base-collection/index.js';

export default class Artifacts extends BaseCollection {
  constructor(items = []) {
    super(items);
    this._taggedItems = new Map();
  }

  add(artifact) {
    if (!artifact.tags || !Array.isArray(artifact.tags)) {
      throw new Error("Artifact must have a tags array");
    }

    const existingTags = new Set(this._taggedItems.keys());
    const newTags = new Set(artifact.tags);

    // Check for conflicting tags
    const conflictingTags = [
      'video:base:small',
      'video:base:large',
      'video:cover:small',
      'video:cover:large',
      'image:base:small',
      'image:base:large'
    ]
      .filter(tag => newTags.has(tag) && existingTags.has(tag));

    // Remove conflicting artifacts
    conflictingTags.forEach(tag => {
      const conflictingArtifact = this._taggedItems.get(tag);
      this._items.delete(conflictingArtifact);
      this._taggedItems.delete(tag);
    });

    // Add new artifact
    super.add(artifact);
    artifact.tags.forEach(tag => this._taggedItems.set(tag, artifact));
  }

  delete(artifact) {
    if (super.delete(artifact)) {
      artifact.tags.forEach(tag => {
        if (this._taggedItems.get(tag) === artifact) {
          this._taggedItems.delete(tag);
        }
      });
      return true;
    }
    return false;
  }

  getByTag(tag) {
    return this._taggedItems.get(tag);
  }

  merge(otherArtifacts) {
    otherArtifacts.toArray().forEach(artifact => this.add(artifact));
    return this;
  }

  all() {
    return this.toArray();
  }
}
