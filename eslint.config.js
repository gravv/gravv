import eslintConfigAirbnbBase from 'eslint-config-airbnb-base';
import eslintConfigPrettier from 'eslint-config-prettier';
import eslintPluginImport from 'eslint-plugin-import';
import eslintPluginJsxA11y from 'eslint-plugin-jsx-a11y';
import globals from 'globals';

export default [
  {
    ignores: ['dist/**', 'node_modules/**', 'bin/**', 'build/**'],
  },
  {
    files: ['**/*.js', '**/*.jsx'],
    languageOptions: {
      ecmaVersion: 'latest',
      sourceType: 'module',
      globals: {
        ...globals.browser,
        ...globals.node,
      },
    },
    plugins: {
      import: eslintPluginImport,
      'jsx-a11y': eslintPluginJsxA11y,
    },
    rules: {
      ...eslintConfigAirbnbBase.rules,
      'no-console': 'error',
      'no-unused-vars': 'error',
      'prefer-const': 'error',
      'indent': ['error', 2],
      'quotes': ['error', 'single', { avoidEscape: true, allowTemplateLiterals: true }],
      'semi': ['error', 'always'],
      'react/jsx-uses-react': 'off',
      'react/react-in-jsx-scope': 'off',
      'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
      'linebreak-style': ['error', 'unix'],
      'comma-dangle': ['error', 'always-multiline'],
      'eol-last': ['error', 'always'],
      'arrow-parens': ['error', 'always'],
      'no-trailing-spaces': 'error',
      'object-curly-spacing': ['error', 'always'],
      'array-bracket-spacing': ['error', 'never'],
      'space-before-function-paren': ['error', 'never'],

      'import/order': ['error', {
	'newlines-between': 'always',
	'groups': ['builtin', 'external', 'internal', 'parent', 'sibling', 'index'],
	'alphabetize': { 'order': 'asc', 'caseInsensitive': true }
      }],
    },
  },
  eslintConfigPrettier,
];
