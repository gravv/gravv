import { configDefaults, defineConfig } from 'vitest/config';

export default defineConfig({
	test: {
    exclude: [...configDefaults.exclude],
		include: ['src/**/*.system.spec.js']
	},
});
