import { configDefaults, defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    exclude: [...configDefaults.exclude],
    include: ['src/**/*.hub-api.spec.js', 'src/**/*.hub-worker.spec.js']
  },
});
